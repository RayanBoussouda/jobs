package jobs.api;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jobs.interfaces.AuthenticationServiceRemote;
import jobs.model.Candidate;
import jobs.model.User;

@Path("authenticate")
public class AuthenticationRessource {
	@EJB
	AuthenticationServiceRemote local ;
	public static User LoggedPerson;	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	 public Response addUser(Candidate cd) {
//		local.ajouterMission(cd);
//			return Response.status(Status.CREATED).entity(cd).build();					
//	}
	@Context
	private UriInfo uriInfo; 

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)

	public Response authenticateUser(@QueryParam("email") String email, @QueryParam("password") String password) {	
			User user = authenticate(email, password);
			if(user.getId()!=0) {
				if(user instanceof Candidate) {
					String token = issueToken(email,user.getId(),"Candidate");
					return Response.ok(token).build();
				}else {
					String token = issueToken(email,user.getId(),"Employe");
					return Response.ok(token).build();
				}
				
				
				
			}else
			 {
				return Response.status(Response.Status.FORBIDDEN).entity("Wrong credentials or not Activated Yet").build();
			}
	}

	private User authenticate(String email, String password) {
		User user = local.authentificationUser(email, password);
		if (user != null) {
			LoggedPerson = user;
		}
		return user;
	}

	private String issueToken(String email,int id,String Role) {
		
		 
		  String keyString = "simplekey";
		  SecretKeySpec key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "DES");
		  System.out.println("the key is : " + key.hashCode()); 
		 
		  System.out.println("uriInfo.getAbsolutePath().toString() : " + uriInfo.getAbsolutePath().toString());
		  System.out.println("Expiration date: " + toDate(LocalDateTime.now().plusMinutes(15L))); 
//		  .setId(Integer.toString(id))
		  String jwtToken = Jwts.builder().setAudience(Role).setId(Integer.toString(id)).setSubject(email).setIssuer(uriInfo.getAbsolutePath().toString()).setIssuedAt(new Date()).setExpiration(toDate(LocalDateTime.now().plusMinutes(15L))).signWith(SignatureAlgorithm.HS512, key).compact(); 
		 
		  System.out.println("the returned token is : " + jwtToken);  
		  return jwtToken;  }

	private Date toDate(LocalDateTime localDateTime) { 
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()); 
		}
	public User getLoggedPerson() {
		if (LoggedPerson == null) {
			return null;
		} else {
			return LoggedPerson;
		}}
}
