package jobs.api;

import javax.ejb.EJB;
import javax.ejb.Timeout;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.EventServiceRemote;
import jobs.model.Event;


@Path("event")
public class EventService {
	@EJB
	EventServiceRemote esr;
	@POST
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	 public Response addEvent(Event ev,@PathParam("id")int id) {
		esr.add(ev, id);
			return Response.status(Status.CREATED).entity(ev).build();					
	}
	@GET	
	@Produces(MediaType.APPLICATION_JSON)
	 public Response listEvents(@QueryParam("id")int id) {
			if(id!=0) {
				return Response.status(Status.CREATED).entity(esr.ListeEventByentreprise(id)).build();					
			}
			return Response.status(Status.CREATED).entity(esr.ListeEvent()).build();					
	}
	@GET
	@Path("participant/{offset}/{limit}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response listParticipants(@QueryParam("id")int id,@PathParam("offset")int offset,@PathParam("limit")int limit) {
				return Response.status(Status.CREATED).entity(esr.ListeParticipant(id,offset,limit)).build();													
	}
	@GET
	@Path("nbrparticipant")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response nbrParticipants(@QueryParam("id")int id) {
				return Response.status(Status.CREATED).entity(esr.sizeParticipant(id)).build();													
	}
	@GET
	@Path("/singleEvent")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response geteventbyid(@QueryParam("id")int id) {
				return Response.status(Status.CREATED).entity(esr.findEventById(id)).build();													
	}
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response deleteEvent(@PathParam("id")int id) {		
		esr.deleteEvent(id);
		return Response.status(Status.ACCEPTED).build();
}
	@PUT
	@Path("/{iduser}/{idevent}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response participate(@PathParam("iduser")int iduser,@PathParam("idevent")int idevent) {
		esr.affectUserToEvent(iduser, idevent);
		return Response.status(Status.ACCEPTED).build();
	}
	@POST
	@Path("/update/{idevent}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateEvent(Event e ,@PathParam("idevent")int idevent) {
		esr.updateEvent(e,idevent);
		return Response.status(Status.ACCEPTED).entity(e).build();
	}
	@GET
	@Path("datee")
	@Produces(MediaType.APPLICATION_JSON)
//	@Timeout
	public Response dateh() {
		esr.sendMailAfterEvent();
		return Response.status(Status.ACCEPTED).build();
	}
}
