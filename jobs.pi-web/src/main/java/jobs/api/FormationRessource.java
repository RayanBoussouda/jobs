package jobs.api;


import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.model.Formation;
import jobs.interfaces.FormationServiceRemote;

@Path("formation")
@RequestScoped
public class FormationRessource {
	
	@EJB
	FormationServiceRemote fs;
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	public int AddFormation(Formation f)
	{
		fs.AddFormation(f);
		return f.getId();
		
		
	}
	
	/*@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String test ()
	{
		return "please?";
	}*/
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all")
	public Response DisplayFormation() {

		return Response.status(Status.ACCEPTED).entity(fs.DisplayFormation()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response FindFormation(@PathParam("id") int id) {
		if (fs.FindFormation(id)!=null) {
		return Response.status(Status.OK).entity(fs.FindFormation(id)).build();}
		else return Response.status(Status.NO_CONTENT).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response updateAnswer(Formation f,@PathParam(value="id")int id){
        Formation x = fs.FindFormation(id);
        x.setPlace(f.getPlace());
        x.setRealisation_date(f.getRealisation_date());
        x.setTitle(f.getTitle());
        
       
		try {
			fs.UpdateFormation(x);
			System.out.println("you good");
			return Response.status(Status.OK).build();			
		} catch (Exception e) {
			System.out.println("you ain't good");
			return Response.status(Status.NOT_MODIFIED).build();
		}

	}
	
	@DELETE
	@Path("{id}")
	public void DeleteFormation(@PathParam("id") int id) {

		fs.DeleteFormation(id);

	}
	
	

}
