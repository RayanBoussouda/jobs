package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.QuizServiceLocal;
import jobs.model.Question;
import jobs.model.Quiz;

@Path("quiz")
public class QuizRessources {
	
	@EJB
	QuizServiceLocal quizRessources;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Quiz> AllQuiz()
	{
		return quizRessources.listQuiz();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Quiz oneQuiz(@PathParam("id")int id)
	{
		return quizRessources.findQuiz(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public int addQuiz (@PathParam(value="id")int id,Quiz q)
	{
		return quizRessources.addQuiz(id,q);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response editQuiz (@PathParam(value="id")int id,Quiz newQ)
	{
		Quiz oldQ = quizRessources.findQuiz(id);
		if(oldQ==null) return Response.status(Status.NOT_FOUND).build();
		oldQ.setDescription(newQ.getDescription());		
		return Response.status(Status.OK).entity(quizRessources.editQuiz(oldQ)).build();
		
	}
	
	@PUT
	@Path("{id_question}/{id_quiz}")
	public Response addQuestion (@PathParam(value="id_question")int id_question,
								@PathParam(value="id_quiz")int id_quiz)
	{
		String rep = quizRessources.addQuestion(id_question, id_quiz);
		if(rep==null) return Response.status(Status.OK).build();
		return Response.status(Status.BAD_REQUEST).entity(rep).build();
	}
	/*@PUT
	@Path("{id_quiz}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addQuestions (List<Integer> questions,
								@PathParam(value="id_quiz")int id_quiz)
	{
		for(int i:questions)
		{
			quizRessources.addQuestion(i, id_quiz);
		}
		 return Response.status(Status.OK).build();
			
	}
	*/
	@DELETE
	@Path("{id}")
	public Response deleteQuestion(@PathParam("id")int id)
	{
		Quiz q = quizRessources.findQuiz(id);
		if(q!=null)
		{
		quizRessources.deleteQuiz(id);
		return Response.status(Status.OK).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

}
