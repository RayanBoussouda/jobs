package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import jobs.interfaces.EntrepriseServiceRemote;
import jobs.interfaces.RatingServiceLocal;
import jobs.model.Claim;
import jobs.model.Entreprise;
import jobs.model.Rating;
import jobs.service.AuthenticationService;

@Path("Rating")
@RequestScoped
public class RatingService {

	@EJB
	RatingServiceLocal rs;
	@EJB
	EntrepriseServiceRemote es;
	AuthenticationService u;

	@POST
	@Path("addRating/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addRatingS(@PathParam("id") int id, Rating r) {

		List<Rating> list = rs.listRatingForEnt(id);
		int test = 0;
		for (Rating rating : list) {

			if (rating.getUser().getId() == u.LoggedPerson.getId()) {

				test = 1;
				break;

			}

			else {

				test = 0;

			}
		}

		if (test == 1) {

			return Response.ok("You alredy rate this entreprise").build();

		} else {

			r.setUser(u.LoggedPerson);
			Entreprise e = es.findEntrepriseById(id);
			r.setEntreprise(e);
			if (r.getStars()<0 || r.getStars()>5) {
				return Response.ok("stars must be between 0 and 5").build();

			}
			else {
				rs.addRating(r);
				return Response.ok("added succefully").build();
			}
		
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public List<Rating> ratinByEntrId(@PathParam("id") int id) {

		return rs.listRatingForEnt(id);
	}
	
	@PUT
	@Path("editRating/{id}")
	public Response update(@PathParam("id") int id, Rating r) {
		Rating ratingToEdit = rs.getRatingById(id);

		if (ratingToEdit.getUser().getId() == u.LoggedPerson.getId()) {

			ratingToEdit.setUser(u.LoggedPerson);
			ratingToEdit.setMessage(r.getMessage());
			ratingToEdit.setTitle(r.getTitle());
			ratingToEdit.setStars(r.getStars());

			if (ratingToEdit.getStars()<0 || ratingToEdit.getStars()>5) {
				return Response.ok("stars must be between 0 and 5").build();

			}
			else {
				rs.editRating(ratingToEdit);
				return Response.ok("Updated").build();
			}


		} else {

			return Response.ok("You cant update other users ratings").build();

		}

	}
	
	@DELETE
	@Path("deleteRating/{id}")
	public Response deletePack(@PathParam("id") int id) {
		Rating r = rs.getRatingById(id);
		if (r.getUser().getId() == u.LoggedPerson.getId()) {

			rs.deleteRating(id);
			return Response.ok("Deleted").build();
		}

		else {

			return Response.ok("you cant delete other users ratings").build();
		}

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)

	@Path("Ratings/{id}")
	public Response ratings(@PathParam("id") int id) {

		double total =  rs.entrepriseRating(id);
		return Response.ok(total).build();
		
		
	}
}
