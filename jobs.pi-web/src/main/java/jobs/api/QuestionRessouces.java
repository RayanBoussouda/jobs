package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.QuestionServiceLocal;
import jobs.model.Question;

@Path("question")
public class QuestionRessouces {
	@EJB
	QuestionServiceLocal questionRessources;
	
	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Question questionById(@PathParam("id") int x) {

		return questionRessources.findQuestion(x);
	}*/
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response allQuestions(@PathParam("id")int id) {

		List<Question> trouve= questionRessources.findAllQuestion(id);
		if(trouve==null) return Response.status(Status.NOT_FOUND).build();
		return Response.status(Status.OK).entity(trouve).build();
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response addQuestion (@PathParam("id")int id,Question q)
	{
		int test =questionRessources.addQuestion(id,q);
	if(test==-1) 
		return Response.status(Status.NOT_FOUND).entity("skill not found").build();
	if(test==0) 
		return Response.status(Status.NOT_ACCEPTABLE).entity("correct answer must be one of the list of answers").build();
	if(test==-2) 
		return Response.status(Status.NOT_ACCEPTABLE).entity("question connot be without answers").build();	
	return Response.status(Status.OK).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response editQuestion (@PathParam(value="id")int id,Question newQ)
	{
		Question oldQ = questionRessources.findQuestion(id);
		if(oldQ==null) return Response.status(Status.NOT_FOUND).build();
		oldQ.setAnswers(newQ.getAnswers());
		oldQ.setQuestion(newQ.getQuestion());
		oldQ.setCorrectAnswer(newQ.getCorrectAnswer());
		
		return Response.status(Status.OK).entity(questionRessources.editQuestion(oldQ)).build();
		
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteQuestion(@PathParam("id")int id)
	{
		Question q = questionRessources.findQuestion(id);
		if(q!=null)
		{
			questionRessources.deleteQuestion(id);
		return Response.status(Status.OK).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/id/{id}")
	public Question questionById(@PathParam("id") int x) {

		return questionRessources.findQuestion(x);
	}

}
