

package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.DiscussionService;
import jobs.interfaces.PostService;
import jobs.model.Discussion;
import jobs.model.Message;

@Path("Discussion")
@RequestScoped
public class DiscussionResource {
	@EJB
	DiscussionService discussionResources;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("allDiscussions")

	public List<Discussion> findAllDiscussion () {
		return discussionResources.viewAllDiscussion();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("allDiscussions/{userId}")

	public List<Discussion> findAllDiscussionUser (@PathParam(value="userId")int userId) {
		return discussionResources.viewAllUserDiscussion(userId);
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("sendMessage/{senderId}/{receiverId}")

	public void sendMessage(Message message,@PathParam(value="senderId")int senderId,@PathParam(value="receiverId")int receiverId) {

		discussionResources.sendMessage(message, senderId, receiverId);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{idDiscussion}/{userId}/messages/all")

	public List<Message> findAllMessagesOfDiscussionUser(@PathParam(value="idDiscussion")int idDiscussion,@PathParam(value="userId")int userId ) {
		return discussionResources.viewAllMessageDiscussion(idDiscussion ,userId);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{idDiscussion}/messages/all")

	public List<Message> findAllMessagesOfDiscussion(@PathParam(value="idDiscussion")int idDiscussion ) {
		return discussionResources.viewAllMessageDiscussion(idDiscussion );
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("messages/all")

	public List<Message> findAllMessages( ) {
		return discussionResources.viewAllMessage();
	}
	
	@PUT
	@Path("{userId}/{DiscussionId}")
	public void deleteConversationRes(@PathParam("userId") int userId,@PathParam("DiscussionId") int DiscussionId) {

		discussionResources.deleteConversation(userId, DiscussionId);

	}
	@DELETE
	@Path("checkMessages")
	public void checkDeletedMessages() {

		discussionResources.checkDeletedMessages();

	}
	@DELETE
	@Path("message/delete/{messageId}")
	public void deleteMessages(@PathParam("messageId") int messageId) {

		discussionResources.deleteMessage(messageId);

	}
	
//	@PUT
//	@Path("{discussionId}/seenUpdate/{receiverId}")
//	public void seenMessage(@PathParam("receiverId") int receiverId,@PathParam("discussionId") int discussionId)
//	{
//		discussionResources.updateSeenOnMessages(discussionId, receiverId);
//	}

}