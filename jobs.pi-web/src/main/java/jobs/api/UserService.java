package jobs.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.decorator.Delegate;
import javax.ejb.EJB;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;


import jobs.RestActivator.Secured;

import org.hibernate.validator.constraints.br.CNPJ;


import jobs.interfaces.UserServiceRemote;
import jobs.model.Candidate;
import jobs.model.CandidateSkills;
import jobs.model.Certifications;
import jobs.model.Employe;

import jobs.model.SuperAdmin;

import jobs.model.Entreprise;
import jobs.model.Formation;

import jobs.service.AuthenticationService;

@Path("User")
public class UserService {
@EJB
UserServiceRemote ur;


AuthenticationService as;
@Context
private UriInfo uriInfo; 
@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
 public Response addUser(Candidate cd) {
	ur.register(cd);
		return Response.status(Status.CREATED).entity(cd).build();					
}



@POST
@Path("emp")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
 public Response addUser(Employe ep) {
	ur.registerE(ep);
		return Response.status(Status.CREATED).entity(ep).build();					
}

@POST
@Path("admin")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
 public Response addUser(SuperAdmin sa) {
	ur.registerA(sa);
		return Response.status(Status.CREATED).entity(sa).build();					
}

@GET
@Path("get")
@Produces(MediaType.APPLICATION_JSON)
public Response getUsers() {
	return Response.status(Status.CREATED).entity(ur.ListeUsers()).build();	
}
@GET
@Path("getById/{id}")
@Produces(MediaType.APPLICATION_JSON)
public Response getUserById(@PathParam("id")int id) {
	return Response.status(Status.CREATED).entity(ur.findUserById(id)).build();	
}
@GET
@Path("activate/{id}")
@Produces(MediaType.APPLICATION_JSON)
public Response activate(@PathParam("id")int id) {
	ur.activate(id);
	return Response.status(Status.OK).entity("your acount has been activated").build();	
}


@PUT
@Path("addskill/{id}/{idskill}")
@Produces(MediaType.APPLICATION_JSON)
 public Response affecterSkill(@PathParam("id")int id,@PathParam("idskill")int idSkill) {
	
	ur.AddSkilltoCandidate(id, idSkill);
 return Response.status(Status.ACCEPTED).entity("skill added").build();				
}

@POST
@Path("addformation")
@Consumes(MediaType.APPLICATION_JSON)
public Response AddFormations(Set<Formation> f)
{
	System.out.println("heey ");
	if(as.LoggedPerson instanceof Candidate)
	{
		Candidate c = (Candidate)as.LoggedPerson;
		
		
		for (Formation formation : f) {
			
			formation.setCandidate(c);
			c.setFormations(f);
			
		}
		ur.updateUser(c);
		 return Response.ok("formations added").build();
	}
	else 
		return Response.ok("access denied").build();
	
}

@POST
@Path("addcertification")
@Consumes(MediaType.APPLICATION_JSON)
public Response AddCertifications(Set<Certifications> cer)
{
	if(as.LoggedPerson instanceof Candidate)
	{
		Candidate c = (Candidate)as.LoggedPerson;
		
		for (Certifications certifications : cer) {
			certifications.setCandidate(c);

			c.setCertifications(cer);
			
		}
		
		ur.updateUser(c);
		 return Response.ok("certification added").build();
	}
	else 
		return Response.ok("access denied").build();
	
}

@PUT
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("{candidateid}")
public Response updateCandidate(Candidate c,@PathParam("candidateid") int idCandidate) {
	
	if(as.LoggedPerson instanceof Candidate && as.LoggedPerson.getId()==idCandidate)
	{
		Candidate c_old = (Candidate)as.LoggedPerson;
		c_old.setNom(c.getNom());
		c_old.setPrenom(c.getPrenom());
		c_old.setEmail(c.getEmail());
		c_old.setPassword(c.getPassword());
		c_old.setBirth_date(c.getBirth_date());
		c_old.setPhone_number(c.getPhone_number());
		c_old.setUsername(c.getUsername());
	
		ur.updateUser(c_old);
		return Response.status(Status.ACCEPTED).entity(c_old).build();	
	}
	else
	return Response.status(Status.ACCEPTED).entity("not updated").build();	
}


@PUT
@Path("addcontact/{idcandidate}/{idcontact}")
public Response AddContact (@PathParam("idcontact") int idcontact,@PathParam("idcandidate") int idCandidate)
{
		Candidate c=(Candidate)ur.findUserById(idCandidate);
		Set<Candidate>ls = c.getContacts();
		if(ls == null)
		{
			ls = new HashSet<Candidate>();
		}
		Candidate contact= (Candidate)ur.findUserById(idcontact);	
		ls.add(contact);
		c.setContacts(ls);
		ur.updateUser(c);
		return Response.ok("contact added").build();		
}

@GET
@Produces(MediaType.APPLICATION_JSON)
@Path("candidatebyname")
public Response getEntrepriseByName(@QueryParam(value="nom") String candidatename) {
 if(ur.findCandidateByName(candidatename).size()!=0) {
		return Response.status(Status.ACCEPTED).entity(ur.findCandidateByName(candidatename)).build();
	}
	else
	return Response.status(Status.NOT_FOUND).entity("candidate not found").build(); 
}

@GET
@Produces(MediaType.APPLICATION_JSON)
@Path("candidatebySkill")
public Response getCandidateBySkill(@QueryParam(value="skill") String skill) {
 if(ur.findCandidateBySkill(skill).size()!=0) {
		return Response.status(Status.ACCEPTED).entity(ur.findCandidateBySkill(skill)).build();
	}
	else
	return Response.status(Status.NOT_FOUND).entity("Candidate with that skill not found").build(); 
}


@GET
@Produces(MediaType.APPLICATION_JSON)
@Path("myContacts")
public Response getMyContacts() {
		
		return Response.status(Status.ACCEPTED).entity(ur.getMyContacts(14)).build();
	
}
//@GET
//@Produces(MediaType.APPLICATION_JSON)
//@Path("myContacts")
//public Response getMyContacts() {
//		
//		return Response.status(Status.ACCEPTED).entity(ur.getMyContacts(as.LoggedPerson.getId())).build();
//	
//}


}






