package jobs.api;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.CandidatureServiceLocal;
import jobs.model.Candidature;
import jobs.model.Question;

@Path("candidature")
public class CandidatureRessource {
	@EJB
	CandidatureServiceLocal candidatureServiceLocal;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id_c}/{id_j}")
	public Response addCandidature (@PathParam("id_c")int id_c,
									@PathParam("id_j")int id_j,
									Candidature c)
	{
		int x=candidatureServiceLocal.addCandidature(id_c, id_j, c);
		if( x==0)
			return Response.status(Status.BAD_REQUEST).build();
		
		return Response.status(Status.OK).entity(x).build();
		
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteC(@PathParam("id")int id)
	{
		if(candidatureServiceLocal.deleteCandidature(id))
			return Response.status(Status.OK).build();
		return Response.status(Status.NOT_FOUND).entity("Candidature inexistante").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("candidate/{id}")
	public Response listByCandidate(@PathParam("id")int id)
	{
		
		return Response.status(Status.OK).entity(candidatureServiceLocal.listCandidatureByCandidate(id)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("offer/{id}")
	public Response listByOffre(@PathParam("id")int id)
	{
		
		return Response.status(Status.OK).entity(candidatureServiceLocal.listCandidatureByOffer(id)).build();
	}
	

}
