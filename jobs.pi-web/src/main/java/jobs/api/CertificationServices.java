package jobs.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.CertificationServiceRemote;
import jobs.model.Certifications;


@Path("certification")
@RequestScoped
public class CertificationServices {
	
	@EJB
	CertificationServiceRemote Cs;
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	public int AddCertification(Certifications c)
	{
		Cs.AddCertification(c);
		return c.getId();	
	}
	@POST
	@Path("addC/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response AddCertif(Certifications c,@PathParam("id") int id)
	{
		Cs.AddCertif(c, id);
			
		return Response.status(Status.ACCEPTED).build();
	}
	
	/*@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String test ()
	{
		return "please?";
	}*/
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all")
	public Response DisplayAddCertification() {

		return Response.status(Status.ACCEPTED).entity(Cs.DisplayCertification()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response FindCertification(@PathParam("id") int id) {
		if (Cs.FindCertification(id)!=null) {
		return Response.status(Status.OK).entity(Cs.FindCertification(id)).build();}
		else return Response.status(Status.NO_CONTENT).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response updateAnswer(Certifications c,@PathParam(value="id")int id){
        Certifications x = Cs.FindCertification(id);
        x.setPlace(c.getPlace());
        x.setAquisition_date(c.getAquisition_date());
        x.setTitle(c.getTitle());
        
       
		try {
			Cs.UpdateCertification(x);
			System.out.println("you good");
			return Response.status(Status.OK).build();			
		} catch (Exception e) {
			System.out.println("you ain't good");
			return Response.status(Status.NOT_MODIFIED).build();
		}

	}
	
	@DELETE
	@Path("{id}")
	public void deleteVehicule(@PathParam("id") int id) {

		Cs.DeleteCertification(id);

	}
	

}
