package jobs.api;

import java.util.List;


import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import jobs.interfaces.NotificationService;
import jobs.model.Notification;


@Path("Notif")
@RequestScoped
public class NotificationResource {
	@EJB(lookup="java:global/jobs.pi-ear/jobs.pi-ejb/NotificationServiceImpl")  
	NotificationService notificationResource;
	
	
	@DELETE
	@Path("{id}")
	public void deleteNotif(@PathParam("id") int id) {
		notificationResource.deleteNotification(id);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("all/{idUser}")
	public List<Notification> findAllUserNotif(@PathParam("idUser") int idUser) {
		System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhh");
		return notificationResource.getUserNotifications(idUser)	;	
	}
	
	

	
}
