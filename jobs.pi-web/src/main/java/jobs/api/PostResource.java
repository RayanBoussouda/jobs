
package jobs.api;

import javax.ejb.EJB;







import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.PostService;
import jobs.model.Comment;
import jobs.model.Likee;
import jobs.model.Post;
import jobs.model.PostType;
import jobs.model.ReplyComment;
import jobs.model.User;

import java.util.List;
import java.util.Map;



@Path("Post")
@RequestScoped
public class PostResource {
	@EJB
	PostService postResources;
	


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("add/{idUser}")
	public void addPost(Post post , @PathParam("idUser") int idUser ) {

		postResources.addPost(post ,idUser);
		

	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all")
	public List<Post> findAllPosts() {

		return postResources.getAll();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("allPostUser/{userId}")
	public List<Post> findAllUserPosts(@PathParam("userId") int userId) {

		return postResources.getAllUserPost(userId);
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getPost/{postId}")
	public Post getPostById(@PathParam("postId") int postId) {

		return postResources.getPostById(postId);
	}
	
	
	@DELETE
	@Path("{id}")
	public void deletePost(@PathParam("id") int id) {

		postResources.deletePost(id);

	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response editPost(Post post,@PathParam(value="id")int id){
		Post x = postResources.getPostById(id);

		try {
			postResources.editPost(post);
			return Response.status(Status.OK).build();
		} catch (Exception e) {
			return Response.status(Status.NOT_MODIFIED).build();
		}

	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("filterCategorie/{categorie}")
	public List<Post> filterPostBasedOnCategory(@PathParam(value="categorie")PostType categorie) {

		return postResources.filterPostsBasedOnCategory(categorie);
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("mostPopularPosts/{days}")
	public List<Post> mostPopularPostsLastDays(@PathParam(value="days")int days) {
		return postResources.MostPopularPostsLastDays(days);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("stats/mostUsedPostCategorie")
	public Map<String, Float> mostUsedPostCategorieStatsRes()
	{
		return postResources.mostUsedPostCategorieStats();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("stats/countPostLast12Months")
	public Map<String, Long> countPostByMonthThisYearStatsRes() {
		return postResources.countPostByMonthThisYearStats();
	}
//------------------------------Comment-----------------------------------
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addComment/{idPost}/{idUser}")
	public void addComment(Comment comment,@PathParam(value="idPost")int idPost, @PathParam("idUser") int idUser ) {
		

		System.out.println( idPost + idUser);
		
		postResources.addComment(comment,idPost,idUser);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("Comment/all/{idPost}")
	public List<Comment> findAllCommentsOfPost(@PathParam(value="idPost")int idPost ) {

		System.out.println(idPost);
		return postResources.getAllPostComment(idPost);
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("Comment/all")
	public List<Comment> findAllCommentsOfPost( ) {

		
		return postResources.getAllComment();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("editComment")
	public Response editComment(Comment comment){

		try {
			postResources.editComment(comment);
			return Response.status(Status.OK).build();
		} catch (Exception e) {
			return Response.status(Status.NOT_MODIFIED).build();
		}

	}
	@DELETE
	@Path("deleteComment/{id}")
	public void deleteComment(@PathParam("id") int id) {

		postResources.deleteComment(id);

	}
	
	//------------------------------Like-----------------------------------
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addLike/{idPost}/{idUser}")
	public void addLike(Likee like,@PathParam(value="idPost")int idPost , @PathParam("idUser") int idUser ) {
				
		postResources.addLike(like, idPost,idUser);
	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addDislike/{idPost}/{idUser}")
	public void addDislike(Likee DisLike,@PathParam(value="idPost")int idPost  , @PathParam("idUser") int idUser ) {
				
		postResources.addDislike(DisLike, idPost, idUser );
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("likes/all/{idPost}")
	public List<Likee> findAllLikesDislikesOfPost(@PathParam(value="idPost")int idPost  ) {

		System.out.println(idPost);
		return postResources.showPeopleLikedDislikedPost(idPost);
	}
	
	@DELETE
	@Path("like/{id}")
	public void deleteLikeDislike(@PathParam("id") int id) {

		postResources.deleteLikeDislike(id);

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("like/{idPost}/{idUser}/{value}")
	public boolean checkIfUserLikedDislikedPost( @PathParam(value="idPost")int idPost ,@PathParam(value="idUser")int idUser , @PathParam(value="value")String value )
	{
		return postResources.checkIfUserLikedDislikedPost(idPost, idUser, value);
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("likeId/{idUser}/{idPost}/{value}")
	public int getLikeDislikeIdByIdUserAndPostAndDelete( @PathParam(value="idUser")int idUser ,@PathParam(value="idPost")int idPost , @PathParam(value="value")String value )
	{
		return postResources.getLikeDislikeIdByIdUserAndPost(idUser,idPost,  value);
	}
	
	
//------------------------------reply-----------------------------------
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addReply/{idComment}/{idUser}")
	public void addReply(ReplyComment rc,@PathParam(value="idComment")int idComment, @PathParam("idUser") int idUser ) {
				
		postResources.addReplyToComment(rc, idComment,idUser);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("reply/all/{idComment}")
	public List<ReplyComment> findAllcommentreplies(@PathParam(value="idComment")int idComment ) {

		return postResources.showCommentReplys(idComment);
	}
	@DELETE
	@Path("reply/{id}")
	public void deletereply(@PathParam("id") int id) {

		postResources.deleteReply(id);

	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("reply/all")
	public List<ReplyComment> findAllcommentreplies( ) {

		return postResources.getAllReplies();
	}
	
}