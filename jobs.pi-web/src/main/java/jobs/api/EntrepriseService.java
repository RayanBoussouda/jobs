package jobs.api;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.RestActivator.Secured;
import jobs.interfaces.EntrepriseServiceRemote;
import jobs.interfaces.UserServiceRemote;
import jobs.model.Candidate;
import jobs.model.Entreprise;


import jobs.service.AuthenticationService;


@Path("entreprise")
public class EntrepriseService {
	@EJB
	EntrepriseServiceRemote er;
	AuthenticationService as;
	UserServiceRemote ur;
	
	@POST
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	 public Response addEntreprise(Entreprise e,@PathParam("id") int id) {
		er.add(e,id);
			return Response.status(Status.CREATED).entity(e).build();					
	}
	@GET
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEntreprises() {
		return Response.status(Status.CREATED).entity(er.ListeEntreprise()).build();	
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response updateEntreprise(Entreprise e) {
		er.updateEntreprise(e);
		return Response.status(Status.ACCEPTED).entity(e).build();	
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getEntreprisebyname")
	public Response getEntrepriseByName(@QueryParam(value="name") String entrepriseName) {
		if(er.findEntrepriseByName(entrepriseName).size()!=0) {
			return Response.status(Status.ACCEPTED).entity(er.findEntrepriseByName(entrepriseName)).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getEntreprisebyfield")
	public Response getEntrepriseByField(@QueryParam("entrepriseField") String entrepriseField) {
		return Response.status(Status.ACCEPTED).entity(er.findEntrepriseByField(entrepriseField)).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getEntreprisebyid/{id}")
	public Response getEntrepriseById(@PathParam("id") int id) {
		return Response.status(Status.ACCEPTED).entity(er.findEntrepriseById(id)).build();
	}
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response deleteEntreprise(@PathParam("id") int id) {
		er.deleteEntreprise(id);
		return Response.status(Status.ACCEPTED).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("employe/{id}")
	public Response listemploye(@PathParam("id") int id) {		
		return Response.status(Status.ACCEPTED).entity(er.listEmploye(id)).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response entreprise(@PathParam("id") int id) {		
		return Response.status(Status.ACCEPTED).entity(er.findEntrepriseById(id)).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("nbr/{id}")
	public Response nbremploye(@PathParam("id") int id) {		
		return Response.status(Status.ACCEPTED).entity(er.nbreEmploye(id)).build();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("followentreprise/{idE}")
	public Response followEntreprise(@PathParam("idE") int idE )
	{
		Set<Candidate> sc;
		Candidate c=(Candidate)as.LoggedPerson;
		Entreprise e=er.findEntrepriseById(idE);

		if (c==null) {
			return Response.ok("noo").build();

		}
		if (e==null) {
			return Response.ok("noo").build();

		}
		if (e.getCandidates()==null) {
			
			sc = new HashSet<Candidate>();
		}
		else {
			sc = e.getCandidates();
		}
	
		System.out.println("********************"+c);
		sc.add(c);
		
		e.setCandidates(sc);
		er.followEntreprise(e, idE);
		System.out.println("********************"+e);
		return Response.ok("ok").build();
			
		
		
	}
}
