package jobs.api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import jobs.interfaces.ClaimServiceLocal;
import jobs.interfaces.PackServiceLocal;
import jobs.interfaces.UserServiceRemote;
import jobs.model.Claim;
import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Pack;
import jobs.model.Role;
import jobs.model.SuperAdmin;
import jobs.model.User;
import jobs.service.AuthenticationService;

@Path("Pack")
@RequestScoped
public class PackService {
	@EJB
	PackServiceLocal ps;
	EntityManager em;

	AuthenticationService u;
	
	private static String crunchifyID = "AW44xaSRPSDA1AliwNFeVd01y75LuxxuwYiL-0iURznALFsz0PwP9d0G_VtY16t5K9RTJWHzvLXdqoa8";
	private static String crunchifySecret = "EPXoZq5UEhDp9rvktRLn5uMIp9AxT_-tDUKfkKMScnbUnf3MnbM7nLlrRcWQBovLO_TsGPaIEGOImi_k";

	private static String executionMode = "sandbox"; // sandbox or production

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addPack")
	public Response addPack(Pack pack) {

		
			ps.addPack(pack);
			return Response.ok("Created").build();



	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AllPacks")
	public List<Pack> findAllPacks() {

		return ps.packs();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Pack packById(@PathParam("id") int id) {

		return ps.getPackById(id);
	}

	@PUT
	@Path("editPack/{id}")
	public Response update(@PathParam("id") int id, Pack pack) {
		Pack packToEdit = ps.getPackById(id);
		if(pack.getDescription()==null) {
			packToEdit.setDescription(packToEdit.getDescription());
		}
		else {
			packToEdit.setDescription(pack.getDescription());

		}
		if(pack.getDescription()==null) {
			packToEdit.setName(packToEdit.getName());
		}
		else {
			packToEdit.setName(pack.getName());

		}
		if(pack.getDescription()==null) {
			packToEdit.setPrice(packToEdit.getPrice());
		}
		else {
			packToEdit.setPrice(pack.getPrice());

		}
		if (u.LoggedPerson instanceof SuperAdmin) {

			ps.editPack(packToEdit);
			return Response.ok("Updated").build();

		} else {

			return Response.ok("Access Denied").build();
		}

	}

	@DELETE
	@Path("deletePack/{id}")
	public Response deletePack(@PathParam("id") int id) {
		if (u.LoggedPerson instanceof SuperAdmin) {
			ps.deletePack(id);
			return Response.ok("Deleted").build();
		} else {

			return Response.ok("Access Denied").build();

		}

	}

	@PUT
	@Path("buyPack/{id}/{user}")
	public Response buyPack(@PathParam("id") int id,@PathParam("user") int user) {

					
					ps.buyPack(id ,user);
					return Response.ok("Pack Added to Your Company with payment info: ").build();

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("PackStatic")
	public Response packStatic() {
			
		return Response.ok(ps.packStatic()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("nbrPack")
	public Response nbrPack() {
			
		return Response.ok(ps.nbrPacks()).build();
	}

}
