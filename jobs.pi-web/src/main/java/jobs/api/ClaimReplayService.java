package jobs.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import jobs.interfaces.ClaimReplayServiceLocal;
import jobs.interfaces.ClaimServiceLocal;
import jobs.model.Claim;
import jobs.model.ClaimReplay;
import jobs.model.SuperAdmin;
import jobs.service.AuthenticationService;

@Path("ClaimReplay")
@RequestScoped
public class ClaimReplayService {
	
	@EJB
	ClaimReplayServiceLocal crs;
	
	AuthenticationService u;
	
	@EJB
	ClaimServiceLocal cs;
	
	@POST
	@Path("addReplay/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addReplay(@PathParam("id") int id,ClaimReplay cr) {
		Claim c = cs.getClaimById(id);
		if (u.LoggedPerson instanceof SuperAdmin) {
			cr.setClaim(c);
			crs.addReplay(cr);
			return Response.ok("Replay added").build();
		}
		else {
			
			return Response.ok("Only admin can replay to claims").build();
			
		}
		

	}
	
	@PUT
	@Path("editReplay/{id}")
	public Response update(@PathParam("id") int id, ClaimReplay replay) {
		ClaimReplay rplayToEdit = crs.getReplayById(id);

		if (u.LoggedPerson instanceof SuperAdmin) {

			
			if (rplayToEdit.getMessage() == null) {
				rplayToEdit.setMessage(rplayToEdit.getMessage());
			} else {
				rplayToEdit.setMessage(replay.getMessage());
			}


			crs.editReplay(rplayToEdit);
			return Response.ok("Updated").build();

		} else {

			return Response.ok("Only admin can edit replay").build();

		}

	}
	
	@DELETE
	@Path("deleteReplay/{id}")
	public Response deletePack(@PathParam("id") int id) {
		ClaimReplay replay = crs.getReplayById(id);
		if (u.LoggedPerson instanceof SuperAdmin) {

			crs.deleteReplay(id);
			return Response.ok("Deleted").build();
		}

		else {

			return Response.ok("Only admin can delete replay").build();
		}

	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("nbrReplays")
	public Response Replays() {

		
		Object c = crs.ReplayStatic();
		return Response.ok(c).build();
	}

}
