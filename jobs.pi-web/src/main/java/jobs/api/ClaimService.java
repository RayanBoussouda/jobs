package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.ClaimServiceLocal;
import jobs.interfaces.UserServiceRemote;
import jobs.model.Claim;
import jobs.service.AuthenticationService;

@Path("Claim")
@RequestScoped
public class ClaimService {

	AuthenticationService u;
	UserServiceRemote users;

	@EJB
	ClaimServiceLocal cs;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("addClaim/{id}")
	public void addClaim(@PathParam("id") int id,Claim cd) {
		cs.addClaim(id,cd);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AllClaims")
	public List<Claim> findAllClaims() {

		return cs.claims();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Claim claimById(@PathParam("id") int id) {

		return cs.getClaimById(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("MyClaims/{id}")
	public List<Claim> claimByUser(@PathParam("id") int id) {

		return cs.getClaimsByUser(id);
	}

	@PUT
	@Path("editClaim/{id}")
	public void update(@PathParam("id") int id, Claim claim) {
		Claim claimToEdit = cs.getClaimById(id);

		if (claimToEdit.getUser().getId() == u.LoggedPerson.getId()) {

			claimToEdit.setUser(u.LoggedPerson);
			if (claim.getType() == null) {
				claimToEdit.setType(claimToEdit.getType());
			} else {
				claimToEdit.setType(claim.getType());
			}
			if (claim.getMessage() == null) {
				claimToEdit.setMessage(claimToEdit.getMessage());

			} else {
				claimToEdit.setMessage(claim.getMessage());

			}
			if (claim.getSubject() == null) {
				claimToEdit.setSubject(claimToEdit.getSubject());

			} else {
				claimToEdit.setSubject(claim.getSubject());

			}

			cs.editClaim(claimToEdit);
			

		} 

	}

	@DELETE
	@Path("deleteClaim/{id}")
	public void deleteClaim(@PathParam("id") int id) {
		Claim claim = cs.getClaimById(id);
		

			cs.deleteClaim(id);
	

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)

	@Path("bugclaim")
	public Response claimStatic() {

		Object bc = cs.bugClaimStatic();
		return Response.ok(bc).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)

	@Path("bugInterview")
	public Response iclaim() {

		Object ic = cs.interClaimStatic();
		return Response.ok(ic).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)

	@Path("bugSocial")
	public Response sclaim() {

		Object sc = cs.socialClaimStatic();
		return Response.ok(sc).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)

	@Path("nbrClaims")
	public Response claims() {

		
		Object c = cs.ClaimStatic();
		return Response.ok(c).build();
	}
	


}
