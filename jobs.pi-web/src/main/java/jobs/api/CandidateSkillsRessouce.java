package jobs.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.CandidateSkillsServiceRemote;

import jobs.model.CandidateSkills;;

@Path("candidateskill")
@RequestScoped
public class CandidateSkillsRessouce {
	@EJB
	CandidateSkillsServiceRemote cs;
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	public int AddCandidateSkill(CandidateSkills c)
	{
		cs.AddCandidateSkill(c);
		return c.getId();
		
		
	}
	
	/*@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String test ()
	{
		return "please?";
	}*/
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all")
	public Response DisplayCandidateSkill() {

		return Response.status(Status.ACCEPTED).entity(cs.DisplayCandidateSkill()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response FindCandidateSkill(@PathParam("id") int id) {
		if (cs.FindCandidateSkill(id)!=null) {
		return Response.status(Status.OK).entity(cs.FindCandidateSkill(id)).build();}
		else return Response.status(Status.NO_CONTENT).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response UpdateCandidateSkill(CandidateSkills c,@PathParam(value="id")int id){
		CandidateSkills x = cs.FindCandidateSkill(id);
        x.setSkill(x.getSkill());
        
       
		try {
			cs.UpdateCandidateSkill(x);
			System.out.println("candidate skill is updated");
			return Response.status(Status.OK).build();			
		} catch (Exception e) {
			System.out.println("candidate skill not updated");
			return Response.status(Status.NOT_MODIFIED).build();
		}

	}
	
	@DELETE
	@Path("{id}")
	public void DeleteCandidateSkill(@PathParam("id") int id) {

		cs.DeleteCandidateSkill(id);

	}
	

}
