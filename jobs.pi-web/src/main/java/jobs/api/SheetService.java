package jobs.api;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.SheetQuickstartServiceRemote;

@Path("sheet")
public class SheetService {
	@EJB
	SheetQuickstartServiceRemote sr;
	@GET

	@Produces(MediaType.APPLICATION_JSON)
	 public Response getJobOffers() throws IOException, GeneralSecurityException {
		
			return Response.status(Status.CREATED).entity(sr.getsheet()).build();				
	}
	@GET
	@Path("stat/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getEventStats(@PathParam("id")int id) throws IOException, GeneralSecurityException {
			return Response.status(Status.CREATED).entity(sr.getStatsOfEvent(id)).build();				
	}
}
