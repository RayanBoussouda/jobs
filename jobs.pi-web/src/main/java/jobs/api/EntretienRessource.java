package jobs.api;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jobs.interfaces.EntretienServiceLocal;

@Path("entretien")
public class EntretienRessource {
	
	@EJB
	EntretienServiceLocal entrtienServiceLocal;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("candidate/{id}")
	public Response listByCandidate(@PathParam("id")int id)
	{
		
		return Response.status(Status.OK).entity(entrtienServiceLocal.listEntretienByCandidate(id)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("offer/{id}")
	public Response listByOffre(@PathParam("id")int id)
	{
		
		return Response.status(Status.OK).entity(entrtienServiceLocal.listEntretienByOffer(id)).build();
	}
	


}
