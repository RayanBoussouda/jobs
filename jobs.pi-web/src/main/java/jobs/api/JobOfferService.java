package jobs.api;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


import jobs.interfaces.OfferJobServiceRemote;

import jobs.model.Job_Offer;

@Path("job")
public class JobOfferService {
	@EJB
	OfferJobServiceRemote oj;
	
	@POST
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	 public Response addJobOffer(Job_Offer jo,@PathParam("id")int id, @QueryParam("skills") List<String> skills) {
		oj.add(jo,id,skills);
			return Response.status(Status.CREATED).entity(jo).build();					
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getJobOffers() {		
			return Response.status(Status.CREATED).entity(oj.ListeJobOffer()).build();				
	}
	@GET
	@Path("getThebest/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getTheBest(@PathParam("id")int id) {		
			return Response.status(Status.CREATED).entity(oj.potentialCandidate(id)).build();				
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getJobOfferById(@PathParam("id")int id) {		
			return Response.status(Status.CREATED).entity(oj.getJobofferById(id)).build();				
	}
	@GET
	@Path("getJobOffers/{id}/{idUser}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getJobOffer(@PathParam("id")int id,@PathParam("idUser")int idUser) {		
			return Response.status(Status.CREATED).entity(oj.findJobOfferById(id, idUser)).build();				
	}
	@GET
	@Path("addVue/{id}/{idUser}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response addVue(@PathParam("id")int id,@PathParam("idUser")int idUser) {		
		oj.addVue(id, idUser);
			return Response.status(Status.CREATED).build();				
	}
	@GET
	@Path("getVues/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response offerVues(@PathParam("id")int id) {		
	
			return Response.status(Status.CREATED).entity(oj.offerVues(id)).build();				
	}
	
	@GET
	@Path("getlisteJobOfferentreprise/{idLoggedUser}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response listeJobOfferOfEntreprise(@PathParam("idLoggedUser")int idLoggedUser) {		
			return Response.status(Status.CREATED).entity(oj.listeJobOfferentreprise(idLoggedUser)).build();				
	}
//	@GET
//	@Path("getJobOffersNotValidate/{idEntreprise}")
//	@Produces(MediaType.APPLICATION_JSON)
//	 public Response ListeJobOfferNotValidate(@PathParam("idEntreprise")int idEntreprise) {		
//			return Response.status(Status.CREATED).entity(oj.ListeJobOfferNotValidate(idEntreprise)).build();				
//	}
	@PUT
	@Path("validateOffer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response validateOffer(@PathParam("id")int id) {		
	 return Response.status(Status.ACCEPTED).entity(oj.validationOffer(id)).build();				
}
	@PUT
	@Path("affecterOffer/{id}/{idoffer}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response affecterSkill(@PathParam("id")int id,@PathParam("idoffer")int idoffer) {
		oj.affecterSkill(id,idoffer);
	 return Response.status(Status.ACCEPTED).build();				
}
	@DELETE
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response deleteOffer(@PathParam("id")int id) {		
		oj.deleteJobOffer(id);
		return Response.status(Status.ACCEPTED).build();
}

}
