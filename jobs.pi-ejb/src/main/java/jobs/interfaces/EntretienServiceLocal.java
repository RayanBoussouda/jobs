package jobs.interfaces;

import java.util.List;

import javax.ejb.Local;

import jobs.model.Entretien;

@Local
public interface EntretienServiceLocal {
	public List<Entretien> listEntretienByOffer(int offer);
	public List<Entretien> listEntretienByCandidate(int candidat);;
	void addEntrtien(int id_c);
	boolean deleteEntretien(int id);

}
