package jobs.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Event;
import jobs.model.User;

@Remote
@LocalBean
public interface EventServiceRemote {
	public int add(Event e,int idEmploye);
	public List<Event> ListeEvent();
	public List<Event> findEventByName(String name);
	public Event findEventById(int id);
	public Event updateEvent(Event event, int idEvent);
	public void deleteEvent(int idEvent) ;
	public List<Event> ListeEventByentreprise(int id);
	public List<User> ListeParticipant(int id,int offset,int limit);
	public void affectUserToEvent(int idUser, int idEvent);
	public void  sendMailAfterEvent();
	public int sizeParticipant(int id);
//	public Map< String,Integer> nbreEmploye(int idEntreprise);
}
