package jobs.interfaces;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.ClaimReplay;

@Remote
@LocalBean
public interface ClaimReplayServiceLocal {

	public int addReplay(ClaimReplay cr);
	public int editReplay(ClaimReplay cr);
	public ClaimReplay getReplayById(int id);
	public void deleteReplay(int id);
	public Object ReplayStatic();
}
