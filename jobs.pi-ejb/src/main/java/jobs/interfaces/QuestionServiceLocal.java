package jobs.interfaces;

import java.util.List;
import java.util.Vector;

import javax.ejb.Local;

import jobs.model.Question;

@Local
public interface QuestionServiceLocal {
	
	public int addQuestion(int id,Question q);
	public int editQuestion(Question q);
	public Vector<Question> listQuestions(String q);
	public boolean deleteQuestion(int id);
	public Question findQuestion(int id);
	public List<Question> findAllQuestion(int id);
	
}
