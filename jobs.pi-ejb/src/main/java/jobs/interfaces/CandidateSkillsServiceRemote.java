package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;


import jobs.model.CandidateSkills;


@Remote
@LocalBean
public interface CandidateSkillsServiceRemote {
	public int AddCandidateSkill(CandidateSkills cs);
	public void DeleteCandidateSkill(int id);
	public void UpdateCandidateSkill(CandidateSkills cs);
	public List<CandidateSkills> DisplayCandidateSkill();
	public CandidateSkills FindCandidateSkill(int id);
	

}
