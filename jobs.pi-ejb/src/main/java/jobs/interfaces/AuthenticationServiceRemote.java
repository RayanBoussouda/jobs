package jobs.interfaces;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Candidate;
import jobs.model.User;
@Remote
@LocalBean
public interface AuthenticationServiceRemote {


//	public int ajouterMission(Candidate cd);


	public User authentificationUser(String login, String password);
}
