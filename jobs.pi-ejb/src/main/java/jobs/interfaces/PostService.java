package jobs.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Comment;
import jobs.model.Likee;
import jobs.model.Post;
import jobs.model.PostType;
import jobs.model.ReplyComment;
import jobs.model.User;

@Remote
@LocalBean
public interface PostService {

	public int addPost (Post post ,int userId);
	public void editPost (Post post);
	public void deletePost (int post);
	public List<Post> getAll();
	public List<Post> getAllUserPost(int userId);
	public Post getPostById(int post_id);
	public List<Post> filterPostsBasedOnCategory(PostType categorie);
	public List<Post> MostPopularPostsLastDays(int days);
	public Map<String , Float >  mostUsedPostCategorieStats(); 
	public Map<String , Long >  countPostByMonthThisYearStats(); 

	//-----------------------comment---------------------------
	public int addComment(Comment comment ,int idpost ,int userId);
	public void editComment(Comment comment);
	public void deleteComment(int id_comment);
	public List<Comment> getAllComment();
	public List<Comment> getAllPostComment(int idPost);
	public Comment getCommentById(int comment_id);
	//-----------------------like------------------------------
	public int addLike(Likee like,int idpost,int userId);
	public void deleteLikeDislike(int like);
	public List<Likee> showPeopleLikedDislikedPost(int idpost  );
	public int addDislike (Likee Dislike,int idpost, int userId);
	public boolean checkIfUserLikedDislikedPost(int idPost ,int idUser ,String value);
	public int getLikeDislikeIdByIdUserAndPost(int idUser , int IdPost, String value);
	//-----------------------reply-----------------------------
	public int addReplyToComment(ReplyComment rc,int idComment,int userId);
	public void deleteReply(int idReply);
	public List<ReplyComment> showCommentReplys(int idComment);
	public List<ReplyComment> getAllReplies();
}
