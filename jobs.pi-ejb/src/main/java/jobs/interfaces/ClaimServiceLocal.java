package jobs.interfaces;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Claim;
import jobs.model.User;
@Remote
@LocalBean
public interface ClaimServiceLocal {
	
	public int addClaim(int id,Claim claim);
	public int editClaim(Claim claim);
	public void deleteClaim(int id_claim);
	public Claim getClaimById(int id_claim) ;
	public List<Claim> claims();
	public List<Claim> getClaimsByUser(int id);
	public Object bugClaimStatic();
	public Object interClaimStatic();
	public Object socialClaimStatic();
	public Object ClaimStatic();



}
