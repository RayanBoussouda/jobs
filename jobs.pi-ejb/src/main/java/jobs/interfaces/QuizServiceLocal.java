package jobs.interfaces;

import java.util.List;

import javax.ejb.Local;

import jobs.model.Quiz;

@Local
public interface QuizServiceLocal {
	
	public int editQuiz(Quiz q);
	public List<Quiz> listQuiz();
	public boolean deleteQuiz(int id);
	public Quiz findQuiz(int id);
	String addQuestion(int id_question, int id_quiz);
	int addQuiz(int o, Quiz q);

}
