package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Formation;
@Remote
@LocalBean
public interface FormationServiceRemote {
	public int AddFormation(Formation f);
	public void DeleteFormation(int id);
	public void UpdateFormation(Formation f);
	public List<Formation> DisplayFormation();
	public Formation FindFormation(int id);

}
