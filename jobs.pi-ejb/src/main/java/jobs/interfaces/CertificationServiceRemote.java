package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Certifications;

@Remote
@LocalBean
public interface CertificationServiceRemote {
	
	public int AddCertification(Certifications c);
	public void DeleteCertification(int id);
	public void UpdateCertification(Certifications c);
	public List<Certifications> DisplayCertification();
	public Certifications FindCertification(int id);
	public int AddCertif(Certifications c, int id);

}
