package jobs.interfaces;

import java.util.List;

import javax.ejb.Local;

import jobs.model.Candidature;
import jobs.model.Job_Offer;
@Local
public interface CandidatureServiceLocal {
	
	public List<Candidature> listCandidatureByOffer(int offer);
	public List<Candidature> listCandidatureByCandidate(int candidat);;
	int addCandidature(int id_c, int id_j, Candidature c);
	boolean deleteCandidature(int id);

}
