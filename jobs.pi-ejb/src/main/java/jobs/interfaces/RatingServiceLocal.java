package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Rating;

@Remote
@LocalBean
public interface RatingServiceLocal {
	
	public int addRating(Rating r);
	public int editRating(Rating r);
	public void deleteRating(int id);
	public double entrepriseRating(int id);
	public Rating getRatingById(int id);
	public List<Rating> listRatingForEnt(int id);

}
