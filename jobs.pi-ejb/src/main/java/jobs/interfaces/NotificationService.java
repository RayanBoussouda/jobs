package jobs.interfaces;

import java.util.List;

import jobs.model.Notification;

public interface NotificationService {
	
	public void deleteNotification (int notifId);
	public List<Notification> getUserNotifications(int userId);
	public void changeSeenNotifs(List <Notification> notifs, int userId);
	
}
