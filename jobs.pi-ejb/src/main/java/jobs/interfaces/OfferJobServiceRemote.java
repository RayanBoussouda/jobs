package jobs.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Entreprise;
import jobs.model.Job_Offer;
import jobs.model.Candidate;

@Remote
@LocalBean
public interface OfferJobServiceRemote {
	public int add(Job_Offer jo,int idEmploye,List<String> skills);
	public List<Job_Offer> ListeJobOffer();
//	public List<Job_Offer> findJobOfferByName(String name);
//	public List<Entreprise> findJobOfferField(String field);
	public Job_Offer findJobOfferById(int id,int idUser);
	public Job_Offer updateJobOffer(Job_Offer jobOffer, int idOffer);
	public void deleteJobOffer(int idOffer);
//	public List<Job_Offer> ListeJobOfferNotValidate(int idEntreprise) ;
	public Boolean validationOffer(int idOffer);
	public int offerVues(int idOffer);
	public void affecterSkill(int idSkill,int idJob) ;
	public Map<String, Integer> potentialCandidate(int idOffer);
	public List<Job_Offer> listeJobOfferentreprise(int idEntreprise);
	public void addVue(int id, int idUser);
	public Job_Offer getJobofferById(int id) ;
}
