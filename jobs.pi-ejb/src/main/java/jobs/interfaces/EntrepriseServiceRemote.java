package jobs.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.User;

@Remote
@LocalBean
public interface EntrepriseServiceRemote {
	public int add(Entreprise e,int id);
	public List<Entreprise> ListeEntreprise();
	public List<Entreprise> findEntrepriseByName(String name);
	public List<Entreprise> findEntrepriseByField(String field);
	public Entreprise findEntrepriseById(int id);
	public Entreprise updateEntreprise(Entreprise entreprise);
	public void deleteEntreprise(int idEntreprise);
	public List<Employe> listEmploye(int idEntreprise);
	public Map< String,Integer> nbreEmploye(int idEntreprise);
	public Entreprise followEntreprise(Entreprise entreprise, int idEntreprise);
	
	
	
	
}
