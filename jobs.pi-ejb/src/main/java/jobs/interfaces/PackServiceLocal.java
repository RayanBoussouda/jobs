package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Entreprise;
import jobs.model.Pack;
@Remote
@LocalBean
public interface PackServiceLocal {

	public int addPack(Pack pack);
	public int editPack(Pack pack);
	public void deletePack(int id_pack);
	public Pack getPackById(int id_pack) ;
	public List<Pack> packs();
	public boolean buyPack(int en, int idUser);
	public Object packStatic();
	public Object nbrPacks();
}
