package jobs.interfaces;

import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Candidate;
import jobs.model.CandidateSkills;
import jobs.model.Employe;
import jobs.model.SuperAdmin;
import jobs.model.User;

@Remote
@LocalBean
public interface UserServiceRemote {
	public int register(Candidate c);
	public int registerA(SuperAdmin sa);
	public List<User> ListeUsers();
	public int registerE(Employe e);
	public int updateUser(User u);
	public User findUserById(int id);
	public List<Candidate> findCandidateByName(String name);
	public void AddSkilltoCandidate(int idSkill,int idCandidate);
	public List<Candidate> findCandidateBySkill(String skill);
	public void activate(int id);
	public Set<Candidate> getMyContacts(int id);

	
}
