package jobs.interfaces;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

import jobs.model.Discussion;
import jobs.model.Message;

@Remote
@LocalBean
public interface DiscussionService {
	public void addDiscussion(Discussion disc); // 
	public void sendMessage(Message message, int senderId ,int receiverId); //
	public List<Message> viewAllMessageDiscussion(int discussionId,int userId); //
	public List<Message> viewAllMessageDiscussion(int discussionId); //
	public List<Discussion> viewAllUserDiscussion(int userId); 
	public void checkDeletedMessages();//
	public void deleteConversation(int userId,int conversationId);
	public void updateSeenOnMessages(int DiscussionId ,int userId); //
	public Discussion getDiscussionById (int discussionId);//
	public Message getMessageById (int messageId);//
	public List<Discussion> viewAllDiscussion();//
	public List<Message> viewAllMessage();
	public void deleteMessage(int messageId);


}
