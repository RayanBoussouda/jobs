package jobs.interfaces;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Remote;

@Remote
@LocalBean
public interface SheetQuickstartServiceRemote {
	 public  List<List<Object>> getsheet() throws IOException, GeneralSecurityException;
	 public Map<String,Map<String,Integer>> getStatsOfEvent(int idEvent) throws IOException, GeneralSecurityException ;
}
