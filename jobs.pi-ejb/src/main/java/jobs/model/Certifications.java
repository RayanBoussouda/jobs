package jobs.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Certifications implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private Date aquisition_date;
	private String place;
	
	@ManyToOne
	@JoinColumn(name = "idCandidate", referencedColumnName = "id", insertable=true, updatable=true)
	Candidate candidate;
	
	
	
	
	public Candidate getCandidate() {
		return candidate;
	}



	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public Date getAquisition_date() {
		return aquisition_date;
	}



	public void setAquisition_date(Date aquisition_date) {
		this.aquisition_date = aquisition_date;
	}



	public String getPlace() {
		return place;
	}



	public void setPlace(String place) {
		this.place = place;
	}



	public Certifications() {
		super();
	}



	public Certifications(int id, String title, Date aquisition_date, String place) {
		super();
		this.id = id;
		this.title = title;
		this.aquisition_date = aquisition_date;
		this.place = place;
	}



	@Override
	public String toString() {
		return "Certifications [id=" + id + ", title=" + title + ", aquisition_date=" + aquisition_date + ", place="
				+ place + "]";
	}
	
	
	
	
	

}
