package jobs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Job_Offer")
public class Job_Offer implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name ;
	@Column(name = "disabled")
	private boolean disabled ;
	@Column(name="entrepriseName")
	private String nameEntreprise;
	@Column(name="description")
	private String description;
	@Column(name="dateCreation")
	private Date dateCreation;
	@Column(name="fieldOfExpertise")
	private String fieldOfExpertise;
	@Column(name="yearsOfExperience")
	private String yearsOfExperience;
	@Column(name="location")
	private String location;
	@Column(name="diploma")
	private String diploma;
	@Column(name="minimumSalary")
	private int minimumSalary;
	@Column(name="maximumSalary")
	private int maximumSalary;
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "idEmploye", referencedColumnName = "id", insertable=true, updatable=true)
	private Employe employe;
	@Column(name="validatedByHR")
	private Boolean validatedByHR;
	@Column(name="available")
	private Boolean available;
	@ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JsonIgnoreProperties({"joboffers"})
	private Set<User> userss=  new HashSet<User>();
	@ManyToMany(cascade = CascadeType.ALL, mappedBy="jo", fetch=FetchType.EAGER)

	private Set<CandidateSkills> skills;
	@OneToOne(cascade=CascadeType.ALL)
	private Quiz quiz;
	
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	public Quiz getQuiz() {
		return quiz;
	}
	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	public Date getDate_entretien() {
		return date_entretien;
	}
	public void setDate_entretien(Date date_entretien) {
		this.date_entretien = date_entretien;
	}
	private Date date_entretien;
	public Set<User> getUserss() {
		return userss;
	}
	public void setUserss(Set<User> userss) {
		this.userss = userss;
	}
	public Set<CandidateSkills> getSkills() {
		return skills;
	}
	public void setSkills(Set<CandidateSkills> skills) {
		this.skills = skills;
	}
	@JsonIgnoreProperties({"joboffers"})
	public Set<User> getUsers() {
		return userss;
	}
	public void setUsers(Set<User> users) {
		this.userss = users;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameEntreprise() {
		return nameEntreprise;
	}
	public void setNameEntreprise(String nameEntreprise) {
		this.nameEntreprise = nameEntreprise;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getFieldOfExpertise() {
		return fieldOfExpertise;
	}
	public void setFieldOfExpertise(String fieldOfExpertise) {
		this.fieldOfExpertise = fieldOfExpertise;
	}
	public String getYearsOfExperience() {
		return yearsOfExperience;
	}
	public void setYearsOfExperience(String yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDiploma() {
		return diploma;
	}
	public void setDiploma(String diploma) {
		this.diploma = diploma;
	}
	public int getMinimumSalary() {
		return minimumSalary;
	}
	public void setMinimumSalary(int minimumSalary) {
		this.minimumSalary = minimumSalary;
	}
	public int getMaximumSalary() {
		return maximumSalary;
	}
	public void setMaximumSalary(int maximumSalary) {
		this.maximumSalary = maximumSalary;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	public Boolean getValidatedByHR() {
		return validatedByHR;
	}
	public void setValidatedByHR(Boolean validatedByHR) {
		this.validatedByHR = validatedByHR;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Job_Offer(String name, String nameEntreprise, String description, Date dateCreation, String fieldOfExpertise,
			String yearsOfExperience, String location, String diploma, int minimumSalary, int maximumSalary,
			Employe employe, Boolean validatedByHR, Boolean available) {
		
		this.name = name;
		this.nameEntreprise = nameEntreprise;
		this.description = description;
		this.dateCreation = dateCreation;
		this.fieldOfExpertise = fieldOfExpertise;
		this.yearsOfExperience = yearsOfExperience;
		this.location = location;
		this.diploma = diploma;
		this.minimumSalary = minimumSalary;
		this.maximumSalary = maximumSalary;
		this.employe = employe;
		this.validatedByHR = validatedByHR;
		this.available = available;
	}
	
	public Job_Offer(int id, String name, String nameEntreprise, String description, Date dateCreation,
			String fieldOfExpertise, String yearsOfExperience, String location, String diploma, int minimumSalary,
			int maximumSalary, Employe employe, Boolean validatedByHR, Boolean available) {
		super();
		this.id = id;
		this.name = name;
		this.nameEntreprise = nameEntreprise;
		this.description = description;
		this.dateCreation = dateCreation;
		this.fieldOfExpertise = fieldOfExpertise;
		this.yearsOfExperience = yearsOfExperience;
		this.location = location;
		this.diploma = diploma;
		this.minimumSalary = minimumSalary;
		this.maximumSalary = maximumSalary;
		this.employe = employe;
		this.validatedByHR = validatedByHR;
		this.available = available;
	}
	@Override
	public String toString() {
		return "Job_Offer [id=" + id + ", name=" + name + ", nameEntreprise=" + nameEntreprise + ", description="
				+ description + ", dateCreation=" + dateCreation + ", fieldOfExpertise=" + fieldOfExpertise
				+ ", yearsOfExperience=" + yearsOfExperience + ", location=" + location + ", diploma=" + diploma
				+ ", minimumSalary=" + minimumSalary + ", maximumSalary=" + maximumSalary + ", employe=" + employe
				+ ", validatedByHR=" + validatedByHR + ", available=" + available + "]";
	}
	public Job_Offer() {
		
	}
	
	
}
