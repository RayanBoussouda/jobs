
package jobs.model;

import java.io.Serializable;



import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;




@Entity
@JsonIgnoreProperties()
public class Post implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7646862633147276028L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String text;
	private String photo_path;
	private String video_path;
	@Enumerated(EnumType.STRING)
	private PostType category;
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date created_at;
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updated_at;

	
	@OneToMany(mappedBy="post",cascade=CascadeType.ALL)
	private List<Likee> likes ; 
	
	
	@OneToMany(mappedBy="post",cascade=CascadeType.ALL)
		private List<Comment> comments;
	

	@ManyToOne 
	private User user ;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getPhoto_path() {
		return photo_path;
	}
	public void setPhoto_path(String photo_path) {
		this.photo_path = photo_path;
	}
	public String getVideo_path() {
		return video_path;
	}
	public void setVideo_path(String video_path) {
		this.video_path = video_path;
	}
	
	
	
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public List<Likee> getLikes() {
		return likes;
	}
	public void setLikes(List<Likee> likes) {
		this.likes = likes;
	}
	
	
	public PostType getType() {
		return category;
	}
	public void setType(PostType category) {
		this.category = category;
	}
	public Post() {
		super();
	}
	
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (created_at == null) {
			if (other.created_at != null)
				return false;
		} else if (!created_at.equals(other.created_at))
			return false;
		if (id != other.id)
			return false;
		if (likes == null) {
			if (other.likes != null)
				return false;
		} else if (!likes.equals(other.likes))
			return false;
		if (photo_path == null) {
			if (other.photo_path != null)
				return false;
		} else if (!photo_path.equals(other.photo_path))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (category != other.category)
			return false;
		if (updated_at == null) {
			if (other.updated_at != null)
				return false;
		} else if (!updated_at.equals(other.updated_at))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (video_path == null) {
			if (other.video_path != null)
				return false;
		} else if (!video_path.equals(other.video_path))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Post [id=" + id + ", text=" + text + ", photo_path=" + photo_path + ", video_path=" + video_path
				+ ", created_at=" + created_at + ", updated_at=" + updated_at + ", likes=" + likes + ", comments="
				+ comments + ", user=" + user + "]";
	}
	
	

}
