package jobs.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Pack implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private  String description;
	private  double price;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="pack",fetch = FetchType.EAGER)
	@JsonIgnore
	private List<Entreprise>entreprise;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public List<Entreprise> getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(List<Entreprise> entreprise) {
		this.entreprise = entreprise;
	}
	
	

	

	public Pack(int id, String name, String description, double price) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		
	}
	public Pack(String name, String description, double price) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	
	
	public Pack(int id, String name, String description, double price, List<Entreprise> entreprise) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.entreprise = entreprise;
	}
	public Pack() {
		
	}
	
	

	
	
	
}
