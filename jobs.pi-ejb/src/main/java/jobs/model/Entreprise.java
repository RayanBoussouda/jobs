package jobs.model;

import java.io.Serializable;

import java.util.ArrayList;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;

import javax.persistence.ManyToMany;

import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Entreprise")
public class Entreprise implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "creationDate")
	private Date date;
	@Column(name = "location")
	private String location;
	@Column(name = "field")
	private String field;

	@Column(name="creationDate",insertable = false, updatable = false)
	private Date creationDate;
	@OneToMany(cascade = CascadeType.REMOVE,mappedBy="entreprise", fetch=FetchType.EAGER)
	@JsonIgnoreProperties({"entrprise","joboffers","password","activated"})	
	private Set<Employe> employes= new HashSet<>();
	@OneToMany(cascade = CascadeType.REMOVE, mappedBy="entreprise", fetch=FetchType.EAGER)
	

	private Set<Event> events;


	@ManyToOne
	private Pack pack;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "entreprise", fetch = FetchType.EAGER)
	private Set<Rating> ratings;


	

	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JsonIgnore
	private Set<Candidate> candidates;
	
	
	
	public Set<Candidate> getCandidates() {
		return candidates;
	}
	public void setCandidates(Set<Candidate> candidates) {
		this.candidates = candidates;
	}


	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@JsonIgnore
	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}

//	@Override
//	public String toString() {
//		return "Entreprise [id=" + id + ", name=" + name + ", date=" + date + ", location=" + location + ", field="
//				+ field + ", creationDate=" + creationDate + ", employes=" + employes + ", events=" + events + ", pack="
//				+ pack + ", ratings=" + ratings + ", candidates=" + candidates + "]";
//	}
	@Override
	public String toString() {
		return "Entreprise [id=" + id + ", name=" + name +"]";
	}
	
	
	

//	public Set<Employe> getEmploye() {
//		return employes;
//	}
//	public void setEmploye(Set<Employe> employes) {
//		employes = employes;
//	}

	

	public void setEmploye(Set<Employe> employes) {
		employes = employes;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public Pack getPack() {
		return pack;
	}

	public void setPack(Pack pack) {
		this.pack = pack;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	@JsonIgnoreProperties({"entrprise","joboffers","password","activated"})
	public Set<Employe> getEmploye() {
		return employes;
	}
//	public void setEmploye(List<Employe> employes) {
//		employes = employes;
//	}

}
