package jobs.model;

public enum DiplomaRole{
	MASTER_DEGREE, NO_DEGREE, ENGINEER_DEGREE, BACHELOR_DEGREE
}