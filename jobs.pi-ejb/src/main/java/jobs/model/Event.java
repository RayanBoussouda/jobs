package jobs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Event")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="location")
	private String location;
	@Column(name="description")
	private String description;
	@Column(name="start_date")
	private Date start_date;
	@Column(name="end_date")
	private Date end_date;
	@Column(name="mailed")
	private Boolean mailed;
	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "idEntreprise", referencedColumnName = "id", insertable=true, updatable=true)
	Entreprise entreprise;
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER )
	private Set<User> users ;
	
	public Boolean getMailed() {
		return mailed;
	}
	public void setMailed(Boolean mailed) {
		this.mailed = mailed;
	}
	@Override
	public String toString() {
		return "Event [id=" + id + ", name=" + name + ", location=" + location + ", description=" + description
				+ ", start_date=" + start_date + ", end_date=" + end_date + ", entreprise=" + entreprise + "]";
	}
	public Event(String name, String location, String description, Date start_date, Date end_date,
			Entreprise entreprise) {
		super();
		this.name = name;
		this.location = location;
		this.description = description;
		this.start_date = start_date;
		this.end_date = end_date;
		this.entreprise = entreprise;
	}
	public Event() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	
	public Entreprise getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
}
