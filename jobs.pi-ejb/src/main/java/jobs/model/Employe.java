package jobs.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name = "Employee")
@DiscriminatorValue(value="Employe")
public class Employe extends User {
	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private Role role;
	@Column(name="dateEmbauche")
	private Date dateEmbauche;
	@ManyToOne(cascade = CascadeType.DETACH)

	@JoinColumn(name = "idEntreprise", referencedColumnName = "id", insertable=true, updatable=true)
	@JsonIgnoreProperties("employe")

//	@JsonBackReference

	Entreprise entreprise = new Entreprise();

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@JsonIgnoreProperties("employe")

	public Entreprise getEntrprise() {
		return entreprise;
	}

	public void setEntrprise(Entreprise entrprise) {
		this.entreprise = entrprise;
	}

	public Employe() {
		
	}
	

	public Date getDateEmbauche() {
		return dateEmbauche;
	}

	public void setDateEmbauche(Date dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}

	public Employe(String nom, String prenom, String email, String password, Role role) {
		super(nom, prenom, email, password);
		this.role=role;
	}

	@Override
	public String toString() {
		return "Employe [role=" + role + ", entreprise=" + entreprise + "]";
	}
	
	
}