package jobs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Candidature implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private Date date;
	
	private ArrayList<Integer> answers;
	
	private float score;
	
	private Result result;
	
	@ManyToOne
	private User candidate;
	
	@ManyToOne
	private Job_Offer job_Offer;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getCandidate() {
		return candidate;
	}

	public void setCandidate(User candidate) {
		this.candidate = candidate;
	}

	public Job_Offer getJob_Offer() {
		return job_Offer;
	}

	public void setJob_Offer(Job_Offer job_Offer) {
		this.job_Offer = job_Offer;
	}

	public Candidature(int id, Date date, Candidate candidate, Job_Offer job_Offer) {
		super();
		this.id = id;
		this.date = date;
		this.candidate = candidate;
		this.job_Offer = job_Offer;
	}

	public Candidature() {
		super();
	}

	public ArrayList<Integer> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<Integer> answers) {
		this.answers = answers;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
	
	
}
