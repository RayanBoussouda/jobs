package jobs.model;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Quiz implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name ;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@ManyToMany(cascade={CascadeType.REMOVE, CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
	private List<Question> questions ;
	
	private String description;
	
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void addQuestion (Question q)
	{
		if (q != null) {
	        if (questions == null) {
	            questions = new Vector<Question>();          
	        }
	        questions.add(q);
	     }
	}
	public Quiz(int id,String name ,String description)
	{
	this.id = id;
	this.description= description;
	this.name= name;
	}
	public Quiz() {
	}
	
	
	

}
