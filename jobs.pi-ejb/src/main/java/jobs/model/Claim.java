package jobs.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Claim implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@ManyToOne
	private User user;
	private String subject;
	@Enumerated(EnumType.STRING)
	private ClaimTypes type;
	private String message;
	private Date date ;
	@OneToOne(cascade=CascadeType.ALL, mappedBy="claim",fetch = FetchType.EAGER)
	private ClaimReplay replay;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public ClaimTypes getType() {
		return type;
	}
	public void setType(ClaimTypes type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	public ClaimReplay getReplay() {
		return replay;
	}
	public void setReplay(ClaimReplay replay) {
		this.replay = replay;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Claim() {
		
	}
	public Claim(int id, User user, String subject, ClaimTypes type, String message, Date date) {
		super();
		this.id = id;
		this.user = user;
		this.subject = subject;
		this.type = type;
		this.message = message;
		this.date = date;
	}
	public Claim(User users, String subject, ClaimTypes type, String message, Date date) {
		super();
		this.user = user;
		this.subject = subject;
		this.type = type;
		this.message = message;
		this.date = date;
	}
	@Override
	public String toString() {
		return "Claim [id=" + id + ", user=" + user + ", subject=" + subject + ", type=" + type + ", message="
				+ message + ", date=" + date + "]";
	}
	public Claim(int id, User user, String subject, ClaimTypes type, String message, Date date, ClaimReplay replay) {
		this.id = id;
		this.user = user;
		this.subject = subject;
		this.type = type;
		this.message = message;
		this.date = date;
		this.replay = replay;
	}


	
	
}
