package jobs.model;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Question implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String question;
	private Vector<String> answers ;
	//@JsonIgnore
	private int correctAnswer;
	
	@ManyToOne
	private CandidateSkills skill;
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Vector<String> getAnswers() {
		return answers;
	}
	public void setAnswers(Vector<String> answers) {
		this.answers = answers;
	}
	public int getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public CandidateSkills getSkill() {
		return skill;
	}
	public void setSkill(CandidateSkills skill) {
		this.skill = skill;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", question=" + question + ", answers=" + answers + ", correctAnswer="
				+ correctAnswer + "]";
	}
	public Question(int id, String question, Vector<String> answers, int correctAnswer, CandidateSkills skill) {
		super();
		this.id = id;
		this.question = question;
		this.answers = answers;
		this.correctAnswer = correctAnswer;
		this.skill=skill;
	}
	public Question() {
		super();
	}
	
	
	
	
	
 
}
