package jobs.model;

import java.io.Serializable;
import java.util.List;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonIgnoreProperties({"favories","likes","comments","posts","discussions1","discussions2","Replies","rating","joboffers","replies"})

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected int id;
	@Column(name = "image")
	protected String image;

	@Column(name = "nom")
	protected String nom;
	@Column(name = "prenom")
	protected String prenom;
	@Column(name = "email")
	protected String email;
	@Column(name = "password")
	protected String password;

	@Column(name="setActivated")
	protected Boolean setActivated;

	@ManyToMany(mappedBy = "userss", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnoreProperties({"employe"})
	private Set<Job_Offer> joboffers;
	@ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JsonBackReference
	@JsonIgnore
	private Set<Event> events;
	@Column(name = "activated")
	protected Boolean activated;

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}
	@JsonIgnoreProperties({"employe"})
	public Set<Job_Offer> getJoboffers() {
		return joboffers;
	}

	public void setJoboffers(Set<Job_Offer> joboffers) {
		this.joboffers = joboffers;
	}


	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Likee> likes;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Comment> comments;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Post> posts;
	@OneToMany(mappedBy = "user1")
	private List<Discussion> discussions1;
	@OneToMany(mappedBy = "user2")
	private List<Discussion> discussions2;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<ReplyComment> Replies;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Claim> claims;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
	private Rating rating;

	public Boolean getSetActivated() {
		return setActivated;
	}

	public void setSetActivated(Boolean setActivated) {
		this.setActivated = setActivated;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;

	}

	@JsonIgnore
	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}

	public List<Discussion> getDiscussions1() {
		return discussions1;
	}

	public void setDiscussions1(List<Discussion> discussions1) {
		this.discussions1 = discussions1;
	}

	public List<Discussion> getDiscussions2() {
		return discussions2;
	}

	public void setDiscussions2(List<Discussion> discussions2) {
		this.discussions2 = discussions2;
	}

	public List<ReplyComment> getReplies() {
		return Replies;
	}

	public void setReplies(List<ReplyComment> Replies) {
		this.Replies = Replies;
	}

	public List<Likee> getLikes() {
		return likes;
	}

	public void setLikes(List<Likee> likes) {
		this.likes = likes;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public Set<Claim> getClaims() {
		return claims;
	}

	public void setClaims(Set<Claim> claims) {
		this.claims = claims;
	}

//	@Override
//	public String toString() {
//		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", password=" + password
//				+ ", setActivated=" + setActivated + ", joboffers=" + joboffers + ", events=" + events + ", activated="
//				+ activated + ", likes=" + likes + ", comments=" + comments + ", posts=" + posts + ", discussions1="
//				+ discussions1 + ", discussions2=" + discussions2 + ", Replies=" + Replies + ", claims=" + claims
//				+ ", rating=" + rating + "]";
//	}
	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom +  "]";
	}
	public User(String nom, String prenom, String email, String password) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
	}

	public User() {

	}

	public User(String nom, String prenom, String email, String password, Set<Claim> claims) {

		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.claims = claims;
	}

}
