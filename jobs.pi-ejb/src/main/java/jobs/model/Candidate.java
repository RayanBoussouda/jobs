package jobs.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Candidate")
@DiscriminatorValue(value="Candidate")
public class Candidate extends User implements Serializable {
	
	private Date birth_date;
	private int phone_number;
	private String username;
	
	@ManyToOne
	@JoinColumn(name="Parent")
	private Candidate parent;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="contacts")
	@JsonIgnoreProperties({"contacts"})
	private Set<Candidate> Contacts;
	
	
	public void addSkill(CandidateSkills skill)
	{
		if (this.skills==null)
			skills=new HashSet<CandidateSkills>();
		
		skills.add(skill);
		
		
	}
	
	@JsonIgnore
	public Candidate getParent() {
		return parent;
	}
	public void setParent(Candidate parent) {
		this.parent = parent;
	}
	@JsonIgnoreProperties({"contacts"})
	public Set<Candidate> getContacts() {
		return Contacts;
	}
	public void setContacts(Set<Candidate> contacts) {
		Contacts = contacts;
	}
//	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
//	private Set<CandidateSkills> skills;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="candidate", fetch=FetchType.LAZY)
	@JsonIgnoreProperties({"candidate"})
	private Set<Certifications> certifications;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="candidate", fetch=FetchType.LAZY)
	private Set<Formation> formations;

	private static final long serialVersionUID = 1L;

	public Candidate() {
		
	}
	@ManyToMany(cascade = CascadeType.ALL, mappedBy="candidates", fetch=FetchType.EAGER)
	@JsonBackReference
	@JsonIgnore
	private Set<CandidateSkills> skills;
	@JsonIgnore
	public Set<CandidateSkills> getSkills() {
		return skills;
	}

	public void setSkills(Set<CandidateSkills> skills) {
		this.skills = skills;
	}

	public Candidate(String nom, String prenom, String email, String password, String status) {
		super(nom, prenom, email, password);
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public int getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(int phone_number) {
		this.phone_number = phone_number;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@JsonIgnoreProperties({"candidate"})
	public Set<Certifications> getCertifications() {
		return certifications;
	}

	public void setCertifications(Set<Certifications> certifications) {
		this.certifications = certifications;
	}

	public Set<Formation> getFormations() {
		return formations;
	}

	public void setFormations(Set<Formation> formations) {
		this.formations = formations;
	}
	
}
