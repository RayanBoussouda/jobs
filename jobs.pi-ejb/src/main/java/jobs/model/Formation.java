package jobs.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Formation implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private Date realisation_date;
	private String place;
	
	@ManyToOne
	@JoinColumn(name = "idCandidate", referencedColumnName = "id", insertable=true, updatable=true)
	Candidate candidate;
	
	
	
	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public String getTitle() {
		return title;
	}	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRealisation_date() {
		return realisation_date;
	}
	public void setRealisation_date(Date realisation_date) {
		this.realisation_date = realisation_date;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	
	@Override
	public String toString() {
		return "Formation [id=" + id + ", title=" + title + ", realisation_date=" + realisation_date + ", place="
				+ place + "]";
	}

	public Formation(String title, Date realisation_date, String place) {
		this.title = title;
		this.realisation_date = realisation_date;
		this.place = place;
	}
	
	public Formation() {
		
	}
	

}
