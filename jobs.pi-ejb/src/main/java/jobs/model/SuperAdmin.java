package jobs.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "superAdmin")
@DiscriminatorValue(value="superAdmin")
public class SuperAdmin extends User {
	private static final long serialVersionUID = 1L;

	 public SuperAdmin() {
		
	}
	public SuperAdmin(String nom, String prenom, String email, String password, String status) {
		super(nom, prenom, email, password);
		
	}
}
