package jobs.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Rating implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private String message;
	private int stars;
	@OneToOne
	private User user;
	@ManyToOne
	@JsonIgnore
	private Entreprise entreprise;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Entreprise getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	public Rating(int id, String title, String message, int stars, User user, Entreprise entreprise) {
		this.id = id;
		this.title = title;
		this.message = message;
		this.stars = stars;
		this.user = user;
		this.entreprise = entreprise;
	}
	public Rating(String title, String message, int stars, User user, Entreprise entreprise) {
	
		this.title = title;
		this.message = message;
		this.stars = stars;
		this.user = user;
		this.entreprise = entreprise;
	}
	
	public Rating() {}
}
