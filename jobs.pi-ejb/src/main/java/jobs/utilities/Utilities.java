package jobs.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class Utilities {
	public static Date getTodayDate() {
		Date todayDate = new Date();
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			todayDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formatter.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return todayDate;
	}
	
	public static String filterBadWords(String str)
	{
		ArrayList<String> badWords = new ArrayList<String>();
	    badWords.add("bad1");
	    badWords.add("bad2");
	    badWords.add("bad3");
	    badWords.add("\\$\\$");
	    
	    for(int i = 0; i < badWords.size(); i++) {
	    	str = str.replaceAll("(?i)" + badWords.get(i), "****");
	    }
	    str = str.replaceAll("\\w*\\*{4}", "****");
	    return str;
	}


}
