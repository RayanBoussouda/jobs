package jobs.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jobs.interfaces.FormationServiceRemote;

import jobs.model.Formation;



@Stateless
public class FormationService implements FormationServiceRemote {
	@PersistenceContext
	EntityManager em;
	
	@Override
	public int AddFormation (Formation f) {		
		em.persist(f);
		return f.getId();		
	}

	@Override
	public void DeleteFormation(int id) {
		em.remove(em.find(Formation.class, id));

	}

	@Override
	public void UpdateFormation(Formation f) {
		em.merge(f);
	}

	@Override
	public List<Formation> DisplayFormation() {
		List<Formation> f = em.createQuery ("SELECT f FROM Formation f",Formation.class).getResultList();
		return f;
	}

	@Override
	public Formation FindFormation(int id) {
		return em.find(Formation.class, id);
	}
	
	
	

}
