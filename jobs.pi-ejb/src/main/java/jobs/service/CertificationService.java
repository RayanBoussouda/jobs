package jobs.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jobs.interfaces.CertificationServiceRemote;
import jobs.model.Candidate;
import jobs.model.Certifications;


@Stateless
public class CertificationService implements CertificationServiceRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public int AddCertification (Certifications c) {		
		em.persist(c);
		return c.getId();		
	}
	@Override 
	public int AddCertif(Certifications c, int id) {
		em.persist(c);
		Candidate  candidate = em.find(Candidate.class, id);
		Certifications certif= em.find(Certifications.class, c.getId());
		candidate.getCertifications().add(c);
		certif.setCandidate(candidate);
		
		return c.getId();
	}

	@Override
	public void DeleteCertification (int id) {
		em.remove(em.find(Certifications.class, id));

	}

	@Override
	public void UpdateCertification(Certifications c) {
		em.merge(c);
	}

	@Override
	public List<Certifications> DisplayCertification() {
		List<Certifications > c = em.createQuery ("SELECT c FROM Certifications c",Certifications.class).getResultList();
		return c;
	}

	@Override
	public Certifications FindCertification(int id) {
		return em.find(Certifications.class, id);
	}

}
