package jobs.service;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import jobs.interfaces.NotificationService;
import jobs.model.Discussion;
import jobs.model.Notification;
import jobs.model.Post;
import jobs.utilities.Utilities;

@Stateless
@LocalBean
public class NotificationServiceImpl implements NotificationService {
	@PersistenceContext(unitName = "jobs-ejb")
	EntityManager em;
	@Override
	public void deleteNotification(int notifId) {
		em.remove(em.find(Notification.class, notifId));

	}

	@Override
	public List<Notification> getUserNotifications(int userId) {
		TypedQuery<Notification> query = em.createQuery("SELECT n FROM Notification n where n.user.id= :userId ORDER BY n.date DESC",Notification.class);
		query.setParameter("userId", userId);
		List<Notification> notifs = query.getResultList();
		List<Notification> notifss=notifs;
		notifs.stream().filter(m -> m.getSeen() == 0 && m.getUser().getId() == userId)
		.forEach(m -> m.setSeen(1));
		
		//changeSeenNotifs(notifs, userId);
		//em.merge(notifs);
		return notifss;
	}

	@Override
	public void changeSeenNotifs(List <Notification> notifs , int userId) {
		try {
			
			notifs.stream().filter(m -> m.getSeen() == 0 && m.getUser().getId() == userId)
					.forEach(m -> m.setSeen(1));
System.out.println("hhh");
			em.merge(notifs);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
