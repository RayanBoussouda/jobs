package jobs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.EntrepriseServiceRemote;
import jobs.model.Candidate;
import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Event;
import jobs.model.Job_Offer;
import jobs.model.Role;
import jobs.model.User;

@Stateless
public class EntrepriseService implements EntrepriseServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public int add(Entreprise e,int id) {
		em.persist(e);
		User u = em.find(User.class,id);
		Query q = em.createNativeQuery("update User u set u.DTYPE='Employe',u.idEntreprise=:ide,u.role='ADMINISTRATEUR' where u.id=:id");
		q.setParameter("ide", e.getId());
		q.setParameter("id", u.getId());
		q.executeUpdate();


//		Query q = em.createNativeQuery("select * from User u where id=1");
//		Object[] a = (Object[]) q.getSingleResult();
//		System.out.println(a[7]);
//		Employe emp = (Employe) AuthenticationService.LoggedPerson;
//		emp.setEntrprise(e);
		return e.getId();
	}

//	@Override

	@Override
	public List<Entreprise> ListeEntreprise() {
		List<Entreprise> e = em.createQuery("SELECT x FROM Entreprise x", Entreprise.class).getResultList();
		return e;
	}
	@Override
	public Entreprise findEntrepriseById(int id) {
		Employe emp = em.find(Employe.class, id);	
		Entreprise e = em.find(Entreprise.class, emp.getEntrprise().getId());
		return e;
	}
	@Override
	public Entreprise updateEntreprise(Entreprise entreprise) {
		Entreprise e = em.find(Entreprise.class, entreprise.getId());
//		e.setId(idEntreprise);
		if (entreprise.getLocation() != null) {

			e.setLocation(entreprise.getLocation());
		}
		if (entreprise.getField() != null) {
			e.setField(entreprise.getLocation());
		}
		if (entreprise.getName() != null) {
			e.setName(entreprise.getName());
		}
		e.setCandidates(entreprise.getCandidates());
		em.merge(e);
		return e;
	}
	@Override
	public Entreprise followEntreprise(Entreprise entreprise, int idEntreprise) {
		Entreprise e = em.find(Entreprise.class, idEntreprise);
		e.setId(idEntreprise);
		if (entreprise.getLocation() != null) {

			e.setLocation(entreprise.getLocation());
		}
		if (entreprise.getField() != null) {
			e.setField(entreprise.getLocation());
		}
		if (entreprise.getName() != null) {
			e.setName(entreprise.getName());
		}
		e.setCandidates(entreprise.getCandidates());
		em.merge(e);
		return e;
	}


	@Override
	public List<Entreprise> findEntrepriseByName(String name) {
		Query query = em.createQuery("select e from Entreprise e where e.name like :nentreprise", Entreprise.class);
		query.setParameter("nentreprise", "%" + name + "%");
		return query.getResultList();

	}



	@Override
	public List<Entreprise> findEntrepriseByField(String field) {
		Query query = em.createQuery("select e from Entreprise e where e.field like :nfield", Entreprise.class);
		query.setParameter("nfield", "%" + field + "%");
		return query.getResultList();
	}

	@Override
	public void deleteEntreprise(int idEntreprise) {
		Query q = em.createNativeQuery("update User u set u.DTYPE='Candidate',u.idEntreprise=null where u.idEntreprise=:id");
		q.setParameter("id", idEntreprise);
		q.executeUpdate();
		Query q2 = em.createNativeQuery("update Event e set e.idEntreprise=null where e.idEntreprise=:id");
		q2.setParameter("id", idEntreprise);
		q2.executeUpdate();
		Entreprise e = em.find(Entreprise.class, idEntreprise);
		
		em.remove(e);
	}
	@Override

	public List<Employe> listEmploye(int id) {
		List<Employe> emp = em.createQuery("select e from Employe e",Employe.class).getResultList();
		System.out.println(emp);
		List<Employe> empfiltred = new ArrayList<>();
		for (Employe ee : emp) {
			if (ee.getEntrprise().getId() == id) {
				empfiltred.add(ee);
			}
		}
		return empfiltred;
	}
	@Override
	public Map<String, Integer> nbreEmploye(int idLoggedUser) {
		List<Employe> emp = em.createQuery("select e from Employe e",Employe.class).getResultList();
		System.out.println(emp);
		List<Employe> empfiltred = new ArrayList<>();
		for (Employe ee : emp) {
			if (ee.getEntrprise().getId() == idLoggedUser) {
				empfiltred.add(ee);
			}
		}
//		Query q = em.createNativeQuery("select count(*) from job_offer where idEmploye in(select id from user where idEntreprise=:idEntreprise)");
//		q.setParameter("idEntreprise",idEntreprise );
//		List<Employe> emp = listEmploye(idEntreprise);
//		Entreprise e = em.find(Entreprise.class, idEntreprise);
		int allemploye = empfiltred.size();
		int rhemploye = 0;
		int teamleader = 0;
		for (Employe ee : emp) {
			if (ee.getRole() == Role.HUMAN_RESSOURCE) {
				rhemploye++;
			} else if (ee.getRole() == Role.TEAM_LEADER) {
				teamleader++;
			}
		}
		Map<String, Integer> hm = new HashMap<String, Integer>();
		hm.put("allEmploye", allemploye);
		hm.put("hr", rhemploye);
		hm.put("teamleader", teamleader);
//		hm.put("nbreOffre", (Integer) q.getSingleResult());
		return hm;
	}

}
