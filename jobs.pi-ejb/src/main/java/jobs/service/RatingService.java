package jobs.service;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.RatingServiceLocal;
import jobs.model.Claim;
import jobs.model.Entreprise;
import jobs.model.Pack;
import jobs.model.Rating;

@Stateless
public class RatingService implements RatingServiceLocal {

	AuthenticationService u;
	EntrepriseService es;
	@PersistenceContext(unitName="jobs-ejb")
	EntityManager em;
	@Override
	public int addRating(Rating r) {

		em.persist(r);
		return r.getId();
	
	}

	@Override
	public int editRating(Rating r) {
		em.merge(r);
		return r.getId();
	}

	@Override
	public void deleteRating(int id) {
		em.remove(em.find(Rating.class, id));
		
	}

	@Override
	public double entrepriseRating(int id) {
		Entreprise e = em.find(Entreprise.class, id);
		Set<Rating> ratings = e.getRatings();
		int lengh = ratings.size();
		double total =0;
		for (Rating rating : ratings) {
			
			total = total + rating.getStars();
		}
		return total/lengh;
	}

	@Override
	public List<Rating> listRatingForEnt(int id) {
		Query query = em.createQuery(
				  "SELECT r FROM Rating r WHERE r.entreprise = '" + id + "'", Rating.class);
		return (List<Rating>) query.getResultList();
		
	}

	@Override
	public Rating getRatingById(int id) {
		return em.find(Rating.class, id);
	}

}
