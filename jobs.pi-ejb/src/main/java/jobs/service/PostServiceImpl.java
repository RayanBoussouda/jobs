package jobs.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import jobs.interfaces.PostService;
import jobs.model.Comment;
import jobs.model.Likee;
import jobs.model.Notification;
import jobs.model.Post;
import jobs.model.PostType;
import jobs.model.ReplyComment;
import jobs.model.User;
import jobs.utilities.Mail;
import jobs.utilities.Utilities;


@Stateless
@LocalBean
public class PostServiceImpl implements PostService {

	@PersistenceContext(unitName = "jobs-ejb")
	EntityManager em;
	@Override
	public int addPost(Post post ,int userId ) {
		post.setUser(em.find(User.class, userId));
		post.setCreated_at(Utilities.getTodayDate());
		
		em.merge(post);
		return post.getId();
	}

	@Override
	public void editPost(Post post) {
		Post p = em.find(Post.class, post.getId()); 
		p.setPhoto_path(post.getPhoto_path());
		p.setVideo_path(post.getVideo_path());
		p.setText(post.getText());
		p.setUpdated_at(Utilities.getTodayDate());
		em.merge(p);

	}

	@Override
	public void deletePost(int post) {
		Post p= em.find(Post.class, post);
		em.remove(p);
		//Mail.sendMail(p.getUser().getEmail(), "your post that was created at "+p.getCreated_at()+" has been deleted by an admin", "Post deleted");

	}


	@Override
	public List<Post> getAll() {

		TypedQuery<Post> query = em.createQuery("SELECT post FROM Post post ",Post.class);	
		List<Post> p = query.getResultList();
		System.out.println(p);
		return p;
	}
	@Override
	public List<Post> getAllUserPost(int userId) {
		System.out.println(userId);
		TypedQuery<Post> query = em.createQuery("SELECT post FROM Post post where post.user.id= :userId ",Post.class);	
		query.setParameter("userId", userId);
		List<Post> p = query.getResultList();
		
		return p;
	}

	@Override
	public Post getPostById(int post_id) {

		return em.find(Post.class, post_id);
	}

	@Override
	public List<Post> filterPostsBasedOnCategory(PostType categorie) {
		TypedQuery<Post> query = em.createQuery("SELECT post FROM Post post where post.category= :categorie",Post.class);
		query.setParameter("categorie", categorie);
		List<Post> p = query.getResultList();
		System.out.println(p);
		return p;

	}
	@Override
	public List<Post> MostPopularPostsLastDays(int days) {
		System.out.println("hhhhhhhhhhhhhhh");
		TypedQuery<Post> query = em.createQuery("SELECT p FROM Post as p where ((TO_DAYS(NOW()) - TO_DAYS(p.created_at)) <= " + days + " )"
				+"ORDER BY (( select COUNT(l) FROM Likee l WHERE l.post.id=p.id) + (select COUNT(c) FROM Comment c where c.post.id=p.id)) DESC",Post.class); 

		List<Post> p = query.getResultList();
		return p;
	}

	@Override
	public Map<String, Float> mostUsedPostCategorieStats() {
		List <String> categories = new ArrayList<>();
		Map<String, Float> stats = new HashMap<String, Float>();
		categories.add("Article");categories.add("Tips");categories.add("Questions");categories.add("Self_Promotion");categories.add("Inspiration");categories.add("News");categories.add("Other");
		int i=0;
		System.out.println("hhhhhhhhhhhhhhhhhhhhh");
		while (categories.size()>=i+1)
		{
			TypedQuery<Long> query = em.createQuery("SELECT COUNT(post) FROM Post post where post.category= :categorie",Long.class);
			query.setParameter("categorie", PostType.valueOf(categories.get(i)));
			long countPostCategory = query.getSingleResult();

			query = em.createQuery("SELECT COUNT(post) FROM Post post",Long.class);
			long countAllPost = query.getSingleResult();
			if (countAllPost>0)
			{
				float percentage=countPostCategory*100 /countAllPost ; 
				System.out.println(percentage);
				stats.put(categories.get(i), percentage );
			}
			i++;
		}
		return stats;
	}
	@Override
	public Map<String, Long> countPostByMonthThisYearStats() {
		Map<String, Long> stats = new LinkedHashMap <String, Long>();
		int i=1;
		while (i<=12)
		{
			TypedQuery<Long> query = em.createQuery("SELECT count(p) FROM Post as p where MONTH(p.created_at) = :monthNb AND YEAR(p.created_at) =:thisYear" ,Long.class);
			query.setParameter("monthNb", i);
			query.setParameter("thisYear", Calendar.getInstance().get(Calendar.YEAR));
			long count = query.getSingleResult();
			LocalDate localDate = LocalDate.of(1, i, 2);


			stats.put(localDate.getMonth().name(), count);
			i++;
		}
		return stats;
	}	




	//-------------------------------------comments----------------------------------------

	

	public int addComment(Comment comment , int idpost ,int userId) {
		comment.setPost(em.find(Post.class, idpost));
		comment.setUser(em.find(User.class, userId));
		comment.setCommentContent(Utilities.filterBadWords(comment.getCommentContent()));	
		comment.setCreated_at(Utilities.getTodayDate());
		//		Mail.sendMail("teststest812@gmail.com", "okkkkkkkkkkk", "hhhhh");
		em.persist(comment);
		Notification notif=new Notification();
		Post p = getPostById(idpost);
		notif.setUser(p.getUser());
		notif.setDate(Utilities.getTodayDate());
		notif.setSeen(0);
		notif.setTitle("Comment");
		notif.setObject("Mr/Mm "+p.getUser().getPrenom()+" " +comment.getUser().getPrenom() +" commented your post");
		em.persist(notif);

		
		return comment.getId();

	}

	@Override
	public void editComment(Comment comment) {
		Comment com = em.find(Comment.class, comment.getId()); 
		com.setCommentContent(Utilities.filterBadWords(comment.getCommentContent())); 
		com.setUpdated_at(Utilities.getTodayDate());
		em.merge(com);


	}

	@Override
	public void deleteComment(int  id_comment) {
		em.remove(em.find(Comment.class, id_comment));
	}

	@Override
	public List<Comment> getAllComment() {
		TypedQuery<Comment> query = em.createQuery("SELECT comment FROM Comment comment",Comment.class);
		List<Comment> p = query.getResultList();
		return p;
	}

	@Override
	public Comment getCommentById(int comment_id) {
		// TODO Auto-generated method stub
		return em.find(Comment.class, comment_id);
	}

	@Override
	public List<Comment> getAllPostComment(int idPost) {
		List<Comment> Comments = new ArrayList<>();
		TypedQuery<Comment> query = em.createQuery(
				"select c from Comment c where c.post=" + idPost + " ORDER BY c.updated_at DESC",
				Comment.class);
		Comments = query.getResultList();
		System.out.println("succes getAllPostComment" );
		return Comments;
	}

	//-------------------------------------Likes----------------------------------------
	@Override
	public int addLike(Likee like,int idpost ,int userId) {
		like.setUser(em.find(User.class, userId));
		like.setPost(em.find(Post.class, idpost));
		like.setValue("like");
		like.setCreated_at(Utilities.getTodayDate());
		em.persist(like);
		Notification notif=new Notification();
		Post p = getPostById(idpost);
		notif.setUser(p.getUser());
		notif.setSeen(0);
		notif.setDate(Utilities.getTodayDate());
		notif.setTitle("Like");
		notif.setObject("Mr/Mm "+p.getUser().getPrenom()+" " +like.getUser().getPrenom() +" Liked your post");
		em.persist(notif);
		return like.getId();
	}
	@Override
	public void deleteLikeDislike(int like) {
		em.remove(em.find(Likee.class, like));
	}
	


	@Override
	public int getLikeDislikeIdByIdUserAndPost(int idUser, int IdPost , String value) {
		Likee like = new Likee();
		TypedQuery<Likee> query = em.createQuery(
				"select l from Likee l where l.post.id=:IdPost and l.user.id=:idUser and l.value=:value ",
				Likee.class);
		query.setParameter("IdPost", IdPost);
		query.setParameter("idUser", idUser);
		query.setParameter("value", value);
		like = query.getSingleResult();
		deleteLikeDislike(like.getId());
		System.out.println("succes getLikeDislikeIdByIdUserAndPost" );
		return like.getId();
		
	}

	@Override
	public int addDislike(Likee Dislike, int idpost , int userId) {
		Dislike.setUser(em.find(User.class, userId));
		Dislike.setPost(em.find(Post.class, idpost));
		Dislike.setValue("dislike");
		Dislike.setCreated_at(Utilities.getTodayDate());
		em.persist(Dislike);
		Notification notif=new Notification();
		Post p = getPostById(idpost);
		notif.setUser(p.getUser());
		notif.setSeen(0);
		notif.setDate(Utilities.getTodayDate());
		notif.setTitle("Dislike");
		notif.setObject("Mr/Mm "+p.getUser().getPrenom()+" " +Dislike.getUser().getPrenom() +" Disliked your post");
		em.persist(notif);
		return Dislike.getId();
	}
	



	@Override
	public List<Likee> showPeopleLikedDislikedPost(int idpost ) {
		List<Likee> likes = new ArrayList<>();
		TypedQuery<Likee> query = em.createQuery(
				"select l  from Likee l where l.post.id= :idPost " ,
				Likee.class);
		query.setParameter("idPost", idpost);
		
		likes = query.getResultList();
		return likes;
	}

	@Override
	public boolean checkIfUserLikedDislikedPost(int idPost,int idUser, String value ) {
		List<Likee> likes = new ArrayList<>();
		TypedQuery<Likee> query = em.createQuery(
				"select l from Likee l where l.post.id= :idPost and l.user.id= :idUser  and l.value=:value" ,
				Likee.class);
		query.setParameter("idPost", idPost);
		query.setParameter("idUser", idUser);
		query.setParameter("value", value);
		likes = query.getResultList();

		return !likes.isEmpty();
	}


	//-------------------------------------Replies-------------------------------------------
	@Override
	public int addReplyToComment(ReplyComment rc, int idComment ,int userId) {
		rc.setUser(em.find(User.class, userId));
		rc.setComment(em.find(Comment.class, idComment));
		rc.setReplyContent(Utilities.filterBadWords(rc.getReplyContent()));
		rc.setCreated_at(Utilities.getTodayDate());
		em.persist(rc);
		return rc.getId();
	}

	@Override
	public void deleteReply(int idReply) {
		em.remove(em.find(ReplyComment.class, idReply));

	}

	@Override
	public List<ReplyComment> showCommentReplys(int idComment) {
		List<ReplyComment> Replies = new ArrayList<>();
		TypedQuery<ReplyComment> query = em.createQuery(
				"select r from ReplyComment r where r.comment.id=" + idComment + " ORDER BY r.updated_at ASC",
				ReplyComment.class);
		Replies = query.getResultList();
		System.out.println("succes getAllRepliesComment" );
		return Replies;
	}

	@Override
	public List<ReplyComment> getAllReplies() {
		TypedQuery<ReplyComment> query = em.createQuery("SELECT r FROM ReplyComment r ORDER BY r.updated_at ASC",ReplyComment.class);
		List<ReplyComment> p = query.getResultList();
		return p;
	}
	





}
