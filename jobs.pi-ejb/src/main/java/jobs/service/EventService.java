package jobs.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.ejb.Schedule;
import javax.ejb.Schedules;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.EventServiceRemote;
import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Event;
import jobs.model.User;
import jobs.utilities.Mail;

@Stateless
public class EventService implements EventServiceRemote {
	@PersistenceContext
	EntityManager em;
	Mail mail;

	@Override
	public int add(Event e, int idEmploye) {
		Employe emp = em.find(Employe.class, idEmploye);
		Entreprise ep = em.find(Entreprise.class, emp.getEntrprise().getId());
		em.persist(e);
		e.setEntreprise(ep);
		e.setMailed(false);
		return emp.getId();

	}

	@Override
	public List<Event> ListeEvent() {
		List<Event> e = em.createQuery("SELECT e FROM Event e", Event.class).getResultList();
		return e;
	}

	@Override
	public List<Event> ListeEventByentreprise(int id) {
		Query query = em.createQuery("SELECT e FROM Event e where idEntreprise=:id", Event.class);
		query.setParameter("id", id);
		return query.getResultList();

	}

	@Override
	public List<Event> findEventByName(String name) {
		Query query = em.createQuery("SELECT e FROM Event e where name=:name", Event.class);
		query.setParameter("name", name);
		return query.getResultList();
		
	}

	@Override
	public Event findEventById(int id) {
		Event e = em.find(Event.class,id);
		return e;
	}

	@Override
	public Event updateEvent(Event event, int idEvent) {
		Event e = em.find(Event.class, idEvent);
		e.setId(idEvent);
//		if (e.getLocation() != null) {
//			e.setLocation(event.getLocation());
//		}
		
		if (e.getName() != null) {
			e.setName(event.getName());
		}
//		if(e.getStart_date()!=null) {
//			e.setStart_date(event.getStart_date());
//		}if(e.getEnd_date()!=null) {
//			e.setEnd_date(event.getEnd_date());
//		}

		return e;
	}

	@Override
	public void deleteEvent(int idEvent)  {
		Event ev = em.find(Event.class, idEvent);
		Query qc = em.createNativeQuery("DELETE  FROM event_user where events_id=:idEvent");
		qc.setParameter("idEvent", idEvent);
		qc.executeUpdate();
		Query q = em.createNativeQuery("DELETE  FROM event where id=:idEvent");
		q.setParameter("idEvent", idEvent);
		q.executeUpdate();
	}

	@Override
	public List<User> ListeParticipant(int id,int offset,int  limit) {

		Event ev = em.find(Event.class, id);
		Set<User> users = ev.getUsers();
		List<User> mainList = new ArrayList<User>();
		mainList.addAll(users);
		List<User> usersFiltred =new ArrayList<User>();
		for(int i=offset;i<offset+limit;i++) {
			usersFiltred.add(mainList.get(i));
		}
		return usersFiltred;
		
	}
	@Override
	public int sizeParticipant(int id) {
		Event ev = em.find(Event.class, id);
		Set<User> u = ev.getUsers();
		return u.size();
	}
	@Override
	public void affectUserToEvent(int idUser, int idEvent) {
		User u = em.find(User.class, idUser);
		Event ev = em.find(Event.class, idEvent);
		Query q = em.createNativeQuery("SELECT a.events_id, a.users_id FROM event_user a");
		List<Object[]> event_user = q.getResultList();
		int exist = 0;
		for (Object[] a : event_user) {
			System.out.println(a[0] + " " + a[1]);
			if ((int) a[0] == idEvent && (int) a[1] == idUser) {

				Query qc = em.createNativeQuery("DELETE  FROM event_user where events_id=:idEvent AND users_id=:idUser");
				qc.setParameter("idEvent", idEvent);
				qc.setParameter("idUser", idUser);
				qc.executeUpdate();
				exist = 1;
			}
		}
		if (exist == 0) {
			Set<User> usersOfEvent = ev.getUsers();
			Set<Event> eventsOfUser = u.getEvents();
			usersOfEvent.add(u);
			eventsOfUser.add(ev);
			u.setEvents(eventsOfUser);
			ev.setUsers(usersOfEvent);
		}

	}

	@Override
//	 @Schedule (hour = "*", minute = "*/1", persistent = false)
	public void sendMailAfterEvent() {
		long timeNow = Calendar.getInstance().getTimeInMillis();
		java.sql.Timestamp ts = new java.sql.Timestamp(timeNow);
		Date nowDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
		List<Event> events = ListeEvent();
		for (Event e : events) {
			System.out.println(ts);
			System.out.println(e.getEnd_date());
			if (ts.after(e.getEnd_date())) {
				List<User> u = ListeParticipant(e.getId());
				for (User user : u) {
					String destmail = user.getEmail();
					String message = "Hi" + user.getNom()
							+ "\n we appreciate your participation to the event and to help us improve and to offer you better services for the next"
							+ "events we would be happy if you would spend few minutes answring our survey https://docs.google.com/forms/d/e/1FAIpQLSe1hxVRT-VWm0kIxuCBfQtexwvitBUrLuofyoDassEcvEP2WA/formResponse";
					String subject = e.getEntreprise().getName();
					mail.sendMail(destmail, message, subject);
				}e.setMailed(true);
			} else {
				System.out.println("noo");
			}
		}
	}
}
