package jobs.service;

import java.util.Calendar;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.ClaimReplayServiceLocal;
import jobs.model.Claim;
import jobs.model.ClaimReplay;

@Stateless
public class ClaimReplayService implements ClaimReplayServiceLocal {

	AuthenticationService u;
	@PersistenceContext(unitName="jobs-ejb")
	EntityManager em;
	
	@Override
	public int addReplay(ClaimReplay cr) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		cr.setDate(date);
		em.persist(cr);		
		return cr.getId();
	}

	@Override
	public int editReplay(ClaimReplay cr) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		cr.setDate(date);
		em.merge(cr);
		return cr.getId();
	}

	@Override
	public ClaimReplay getReplayById(int id) {
		return em.find(ClaimReplay.class, id);
	}

	@Override
	public void deleteReplay(int id) {
		em.remove(em.find(ClaimReplay.class, id));
		
	}

	@Override
	public Object ReplayStatic() {
		Object Claims;
		Query query = em.createQuery("SELECT COUNT(c) FROM ClaimReplay c");
		Claims = query.getSingleResult();

		return Claims;
	}

}
