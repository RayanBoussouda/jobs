package jobs.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.CandidatureServiceLocal;
import jobs.model.Candidature;
import jobs.model.Entretien;
import jobs.model.Job_Offer;
import jobs.model.Question;
import jobs.model.Result;
import jobs.model.User;
import jobs.utilities.Mail;

@Stateless
public class CandidatureService implements CandidatureServiceLocal {

	@PersistenceContext
	EntityManager em;
	
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");

	@Override
	public int addCandidature(int id_c, int id_j ,Candidature c) 
	{	
		Query query = em.createQuery("select c from Candidature c where "
									+ "c.candidate.id="+id_c
									+" and c.job_Offer.id="+id_j);
		List<Candidature> test = (List<Candidature>) query.getResultList();
		User candidate = em.find(User.class, id_c);
		Job_Offer offer = em.find(Job_Offer.class, id_j);
		if(candidate==null || offer==null || test.size()>0 || c.getAnswers()==null ||
				c.getAnswers().size()!=offer.getQuiz().getQuestions().size() )
		return 0;
		float score=calculScore(c.getAnswers(),offer.getQuiz().getQuestions());
		c.setScore(score);
		if(score>=50) c.setResult(Result.accepted);
		else c.setResult(Result.refused);
		c.setCandidate(candidate);
		c.setJob_Offer(offer);
		c.setDate(new Date());
		em.persist(c);
		if(score>=50)
		{
			Entretien entretien = new Entretien();
			entretien.setCandidature(c);
			entretien.setDate(c.getJob_Offer().getDate_entretien());
			em.persist(entretien);
			String message = "Bonjour,\n"
					+ "Nous avons le plaisir de vous informer que votre candidature"
					+ " est acceptée et que vous êtes invité a passer l'entretien le "
					+date.format(c.getJob_Offer().getDate_entretien())
					 +".\n"
					+"Cordialement."+"\n\n"
					+ "Ceci est un mail automatique merci de ne pas répondre";
			Mail.sendMail(c.getCandidate().getEmail(), message, "entretien");
		}
		return c.getId();
	}

	private float calculScore(List<Integer> answers, List<Question> questions) {
		
		int r=0;
		for(int i=0;i<questions.size();i++)
		{
			if(questions.get(i).getCorrectAnswer()==answers.get(i)) 
				r++;
		}
		return ((float)(r*100/questions.size()));
	}

	@Override
	public List<Candidature> listCandidatureByOffer(int offer) {
		Query query = em.createQuery("select c from Candidature c where "
									+ "c.job_Offer.id="+offer);
		return (List<Candidature>)query.getResultList();
	}

	@Override
	public List<Candidature> listCandidatureByCandidate(int candidat) {
		Query query = em.createQuery("select c from Candidature c where "
				+ "c.candidate.id="+candidat);
return (List<Candidature>)query.getResultList();
	}

	@Override
	public boolean deleteCandidature(int id) {
		Candidature candidature = em.find(Candidature.class, id);
		if(candidature!=null) 
		{
			Query query = em.createQuery("select e from Entretien e where "
				+ "e.candidature.id="+id);
			if( query.getResultList().size()!=0) em.remove(query.getSingleResult());
			em.remove(candidature);
			return true;
		}
		return false;
	}


	
	

}
