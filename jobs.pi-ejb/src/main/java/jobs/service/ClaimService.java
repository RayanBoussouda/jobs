package jobs.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.ClaimServiceLocal;
import jobs.interfaces.UserServiceRemote;
import jobs.model.Claim;
import jobs.model.User;

@Stateless
public class ClaimService implements ClaimServiceLocal {

	AuthenticationService u;
	UserServiceRemote user;
	@PersistenceContext(unitName="jobs-ejb")
	EntityManager em;
	@Override
	public int addClaim(int id,Claim claim) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		claim.setUser(em.find(User.class, id));
		claim.setDate(date);
		em.persist(claim);
		return claim.getId();
	}

	@Override
	public int editClaim(Claim claim) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		claim.setUser(u.LoggedPerson);
		claim.setDate(date);
		em.merge(claim);
		return claim.getId();
		}
	
	@Override
	public Claim getClaimById(int id_claim) {
		return em.find(Claim.class, id_claim);
	}

	@Override
	public void deleteClaim(int id_claim) {
	
			
			em.remove(em.find(Claim.class, id_claim));

		
	}

	@Override
	public List<Claim> claims() {
		Query query = em.createQuery(
				"SELECT c FROM Claim c");
		return (List<Claim>) query.getResultList();
	}

	@Override
	public List<Claim> getClaimsByUser(int id) {
		User u = em.find(User.class, id);
		Query query = em.createQuery(
				  "SELECT c FROM Claim c WHERE c.user = '" + u.getId() + "'", Claim.class);
		return (List<Claim>) query.getResultList();
	}



	@Override
	public Object bugClaimStatic() {
		Object bugClaim;
		Query query = em.createQuery("SELECT COUNT(c) FROM Claim c WHERE c.type='" + "BUG" + "'");
		bugClaim = query.getSingleResult();
		return bugClaim;
	}

	@Override
	public Object interClaimStatic() {
		Object interviewClaim;
		Query query1 = em.createQuery("SELECT COUNT(c) FROM Claim c WHERE c.type='" + "INTERVIEW" + "'");
		interviewClaim = query1.getSingleResult();
		return interviewClaim;
	}

	@Override
	public Object socialClaimStatic() {
		Object socialClaim;
		Query query2 = em.createQuery("SELECT COUNT(c) FROM Claim c WHERE c.type='" + "SOCIAL_CLAIM" + "'");
		socialClaim = query2.getSingleResult();

		return socialClaim;
	}

	@Override
	public Object ClaimStatic() {
		Object Claims;
		Query query = em.createQuery("SELECT COUNT(c) FROM Claim c");
		Claims = query.getSingleResult();

		return Claims;
	}

}
