package jobs.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.OfferJobServiceRemote;
import jobs.model.Candidate;
import jobs.model.CandidateSkills;
import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Event;
import jobs.model.Job_Offer;
import jobs.model.Role;
import jobs.model.User;

@Stateless
public class OfferJobService implements OfferJobServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public int add(Job_Offer jo, int idEmploye,List<String>skills) {
		em.persist(jo);
		int idJo=jo.getId();
		String lista= skills.get(0);
		List<Integer> listamtaamele5er = new ArrayList<Integer>(); 
		for(int i=0;i<lista.length();i++) {
			if(lista.charAt(i)!=',') {
				listamtaamele5er.add(Character.getNumericValue(lista.charAt(i)));
			}
		}
//		System.out.println("lista"+listamtaamele5er);
//		System.out.println(listamtaamele5er.size());

		List<CandidateSkills> f = em.createQuery ("SELECT cs FROM CandidateSkills cs",CandidateSkills.class).getResultList();
		Set<CandidateSkills> skillsToAdd = new HashSet<CandidateSkills>();
		for(int i=0;i<listamtaamele5er.size();i++) {
			Query q = em.createNativeQuery("INSERT INTO  candidateskills_job_offer values (:idSkill,:idJo)");
			q.setParameter("idJo", idJo);
			q.setParameter("idSkill",listamtaamele5er.get(i));
			q.executeUpdate();
		}
		
		System.out.println(skillsToAdd);
		Employe employe = em.find(Employe.class, idEmploye);
		Entreprise e = em.find(Entreprise.class, employe.getEntrprise().getId());
		jo.setAvailable(true);
		jo.setNameEntreprise(e.getName());
		jo.setEmploye(employe);
		jo.setDisabled(false);
		Timestamp timestamp = new Timestamp(new Date().getTime());
		jo.setDateCreation(timestamp);
		if (employe.getRole() == Role.TEAM_LEADER) {
			jo.setValidatedByHR(false);
		} else
			jo.setValidatedByHR(true);
		return jo.getId();
	}

	@Override
	public List<Job_Offer> ListeJobOffer() {
		List<Job_Offer> e = em.createQuery("SELECT x FROM Job_Offer x", Job_Offer.class).getResultList();
		return e;
	}
	
	@Override
	public Job_Offer getJobofferById(int id) {
		Job_Offer jo= em.find(Job_Offer.class, id);
		return jo;
	}
	@Override
	public List<Job_Offer> listeJobOfferentreprise(int idLoggedUser){
		Employe employe = em.find(Employe.class, idLoggedUser);
		List<Job_Offer> j = ListeJobOffer();
//		Query q = em.createNativeQuery("select * from job_offer where idEmploye in(select id from user where idEntreprise=:idEntreprise)");
//		q.setParameter("idEntreprise",idEntreprise );
		List<Job_Offer> jf = new ArrayList<Job_Offer>();
		for(int i=0;i<j.size();i++) {
			if(j.get(i).getEmploye().getEntrprise().getId()==employe.getEntrprise().getId()) {
				jf.add(j.get(i));
			}
		}
		
		
		return jf;
	}
//	@Override
//	public List<Job_Offer> ListeJobOfferNotValidate(int idEntreprise) {
//		Query q = em.createNativeQuery("select validatedByHR from job_offer where idEmploye in(select id from user where idEntreprise=:idEntreprise)");
//		q.setParameter("idEntreprise",idEntreprise );
//		List<Boolean> a = q.getResultList();
//		List<Job_Offer> ljo=listeJobOfferentreprise(idEntreprise);
//		List<Job_Offer> e =new ArrayList<Job_Offer>();
//		//System.out.println(listeJobOfferentreprise(idEntreprise));
//		for(int i=0;i<ljo.size();i++) {
//			if(a.get(i)==false) {
//				e.add(ljo.get(i));
//			}
//		}
////		for(int i=0;i<ljo.size();i++) {
////			if(ljo.get(i).getValidatedByHR()==false) {
////				System.out.println("...");
////			}
////		}
////		for(Job_Offer jo:listeJobOfferentreprise(idEntreprise)) {
////			if(jo.getValidatedByHR()==false) {
////				System.out.println(jo);
////			e.add(jo);
////			}
////		}
////		List<Job_Offer> e = em.createQuery("SELECT x FROM Job_Offer x where x.validatedByHR=FALSE", Job_Offer.class)
////				.getResultList();
//		return ljo;
//	}

	@Override
	public Job_Offer findJobOfferById(int id, int idUser) {
		Job_Offer jo = em.find(Job_Offer.class, id);
		User u = em.find(User.class, idUser);
		Set<User> us = jo.getUsers();
		Set<Job_Offer> jobs = u.getJoboffers();
		us.add(u);
		jobs.add(jo);
		u.setJoboffers(jobs);
		jo.setUsers(us);
		return jo;
	}
	@Override
	public void addVue(int id, int idUser) {
		Job_Offer jo = em.find(Job_Offer.class, id);
		User u = em.find(User.class, idUser);
		Set<User> us = jo.getUsers();
		Set<Job_Offer> jobs = u.getJoboffers();
		us.add(u);
		jobs.add(jo);
		u.setJoboffers(jobs);
		jo.setUsers(us);	
	}


	@Override
	public Job_Offer updateJobOffer(Job_Offer jobOffer, int idOffer) {
		Job_Offer e = em.find(Job_Offer.class, idOffer);
		
		e.setId(idOffer);
		if (e.getLocation() != null) {
			e.setLocation(jobOffer.getLocation());
		}
		
		if (e.getName() != null) {
			e.setName(jobOffer.getName());
		}
		if(e.getDescription()!=null) {
			e.setDescription(jobOffer.getDescription());
		}if(e.getMinimumSalary()!=0) {
			e.setMinimumSalary(jobOffer.getMinimumSalary());
		}
		if(e.getMaximumSalary()!=0) {
			e.setMaximumSalary(jobOffer.getMaximumSalary());
		}if(e.getYearsOfExperience()!=null) {
			e.setYearsOfExperience(jobOffer.getYearsOfExperience());
		}

		return e;
	}

	@Override
	public void deleteJobOffer(int idOffer) {
		Job_Offer jo = em.find(Job_Offer.class, idOffer);
		jo.setDisabled(true);
	}

	@Override
	public Boolean validationOffer(int idOffer) {
		Job_Offer j = em.find(Job_Offer.class, idOffer);
		if (j.getValidatedByHR() == true) {
			j.setValidatedByHR(false);
		} else if (j.getValidatedByHR() == false) {
			j.setValidatedByHR(true);
		}

		return j.getValidatedByHR();
	}

	@Override
	public int offerVues(int idOffer) {
		Job_Offer j = em.find(Job_Offer.class, idOffer);
		return j.getUsers().size();
	}

	@Override
	public void affecterSkill(int idSkill, int idJob) {
		CandidateSkills cs = em.find(CandidateSkills.class, idSkill);
		Job_Offer jo = em.find(Job_Offer.class, idJob);
		System.out.println(jo);
		System.out.println(cs);
		Set<CandidateSkills> css = jo.getSkills();
		css.add(cs);	
		jo.setSkills(css);
	}

	@Override
	public Map<String, Integer> potentialCandidate(int idOffer) {
		Job_Offer jo = em.find(Job_Offer.class, idOffer);
		List<Candidate> allcandidate = em.createQuery("SELECT x FROM Candidate x", Candidate.class).getResultList();
		Map<String, Integer> hm = new HashMap<String, Integer>();
		for (Candidate allc : allcandidate) {
			int x = 0;
			for (CandidateSkills j : jo.getSkills()) {
				List<Candidate> bestCandidate = new ArrayList<Candidate>();
				int idd=0;
				System.out.println(allc.getId());
				System.out.println(allc.getSkills());
				for (CandidateSkills skil : allc.getSkills()) {
					if (skil.getId() == j.getId()) {
						x++;
						bestCandidate.add(allc);
						hm.put(allc.getEmail(), x);
					}
				}
			}
		}
		
		return hm;
	}

}
