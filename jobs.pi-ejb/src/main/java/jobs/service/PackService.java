package jobs.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import jobs.interfaces.PackServiceLocal;
import jobs.model.Claim;
import jobs.model.Employe;
import jobs.model.Entreprise;
import jobs.model.Pack;
import jobs.model.Role;
import jobs.model.User;

@Stateful
public class PackService implements PackServiceLocal {
	



	@PersistenceContext(unitName = "jobs-ejb")
	EntityManager em;

	@Override
	public int addPack(Pack pack) {
		em.persist(pack);
		return pack.getId();
	}

	@Override
	public int editPack(Pack pack) {
		em.merge(pack);
		return pack.getId();
	}

	@Override
	public void deletePack(int id_pack) {
		em.remove(em.find(Pack.class, id_pack));

	}

	@Override
	public Pack getPackById(int id_pack) {
		return em.find(Pack.class, id_pack);

	}

	@Override
	public List<Pack> packs() {
		Query query = em.createQuery("SELECT p from Pack p");
		return (List<Pack>) query.getResultList();
	}

	@Override
	public boolean buyPack(int p, int idUser) {

		Pack pack = em.find(Pack.class, p);
		User user = em.find(User.class, idUser);
		Employe u = (Employe)user;
		Entreprise en = u.getEntrprise();
		en.setPack(pack);
		em.merge(en);
		return true;
		

	}

	private void log(String string) {
		System.out.println(string);
 
	}
	@Override
	public Object packStatic() {
		
		
			
			Object count = 0;
			Query query1 = em.createQuery("SELECT COUNT(e) FROM Entreprise e INNER JOIN Pack p ON e.pack = p");
			count = query1.getSingleResult();
			System.out.println(count);
			
			
		
		
		return count;
	}
	
	@Override
	public Object nbrPacks() {
		
		
			
			Object count = 0;
			Query query1 = em.createQuery("SELECT COUNT(p) FROM Pack p");
			count = query1.getSingleResult();
			System.out.println(count);
			
			
		
		
		return count;
	}

}
