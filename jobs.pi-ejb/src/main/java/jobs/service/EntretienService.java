package jobs.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.EntretienServiceLocal;
import jobs.model.Candidature;
import jobs.model.Entretien;

@Stateless
public class EntretienService implements EntretienServiceLocal {
	@PersistenceContext
	EntityManager em;

	@Override
	public List<Entretien> listEntretienByOffer(int offer) {
		Query query = em.createQuery("select e from Entretien e where "
				+ "e.candidature.job_Offer.id="+offer);
return (List<Entretien>)query.getResultList();
	}

	@Override
	public List<Entretien> listEntretienByCandidate(int candidat) {
		Query query = em.createQuery("select e from Entretien e where "
				+ "e.candidature.candidate.id="+candidat);
return (List<Entretien>)query.getResultList();
	}

	@Override
	public void addEntrtien(int id_c) {
			Candidature candidature = em.find(Candidature.class, id_c);
			Entretien entretien= new Entretien();
			entretien.setDate(new Date());
			entretien.setCandidature(candidature);
			em.persist(entretien);
		
	}

	@Override
	public boolean deleteEntretien(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
