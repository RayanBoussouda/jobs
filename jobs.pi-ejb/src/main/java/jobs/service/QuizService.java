package jobs.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.QuizServiceLocal;
import jobs.model.Job_Offer;
import jobs.model.Question;
import jobs.model.Quiz;

@Stateless
public class QuizService implements QuizServiceLocal {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public int addQuiz(int o,Quiz q) 
	{	
		System.out.println(q.getId());
		Job_Offer job_Offer= em.find(Job_Offer.class,o );
		if(job_Offer!=null)
		{
		job_Offer.setQuiz(q);
		em.merge(job_Offer);
		return job_Offer.getQuiz().getId();
		}
		return q.getId();
	}

	@Override
	public int editQuiz(Quiz q) {
		em.merge(q);
		return q.getId();
	}
	
	@Override
	public String addQuestion(int id_question,int id_quiz) {
		Question question = em.find(Question.class, id_question);
		Quiz quiz = em.find(Quiz.class, id_quiz);
		if(question==null) return "question inexistant";
		if(quiz==null) return "quiz inexistant";
		/*Query query = em.createQuery("select a from quiz_question a where"
				+ "a.Quiz_id="+id_quiz+"and a.questions_id="+id_question);*/
		quiz.addQuestion(question);
		return null;
	}

	@Override
	public List<Quiz> listQuiz() {
		Query query = em.createQuery(
				"SELECT new Quiz(u.id,u.name,u.description) "
						+ "FROM Quiz u");
		return (List<Quiz>) query.getResultList();
	}

	@Override
	public boolean deleteQuiz(int id) {
		
		em.remove(em.find(Quiz.class, id));
		return false;
	}

	@Override
	public Quiz findQuiz(int id) {
		
		return em.find(Quiz.class, id);
	}

}
