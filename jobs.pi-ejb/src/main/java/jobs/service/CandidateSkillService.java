package jobs.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jobs.interfaces.CandidateSkillsServiceRemote;
import jobs.model.CandidateSkills;

@Stateless
public class CandidateSkillService implements CandidateSkillsServiceRemote {
	@PersistenceContext
	EntityManager em;
	
	@Override
	public int AddCandidateSkill(CandidateSkills cs) {		
		em.persist(cs);
		return cs.getId();		
	}

	@Override
	public void DeleteCandidateSkill(int id) {
		em.remove(em.find(CandidateSkills.class, id));

	}

	@Override
	public void UpdateCandidateSkill(CandidateSkills cs) {
		em.merge(cs);
	}

	@Override
	public List<CandidateSkills> DisplayCandidateSkill() {
		List<CandidateSkills> f = em.createQuery ("SELECT cs FROM CandidateSkills cs",CandidateSkills.class).getResultList();
		return f;
	}

	@Override
	public CandidateSkills FindCandidateSkill(int id) {
		return em.find(CandidateSkills.class, id);
	}
	

}
