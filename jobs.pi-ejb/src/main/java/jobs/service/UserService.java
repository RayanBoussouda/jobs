package jobs.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.resource.spi.SecurityPermission;

import jobs.interfaces.UserServiceRemote;
import jobs.model.Candidate;
import jobs.model.CandidateSkills;
import jobs.model.Employe;
import jobs.model.Entreprise;

import jobs.model.SuperAdmin;

import jobs.model.Job_Offer;

import jobs.model.User;
import jobs.utilities.CryptPasswordMD5;
import jobs.utilities.Mail;

@Stateless
public class UserService implements UserServiceRemote {
	Mail mail;

	@PersistenceContext
	EntityManager em;

	private CryptPasswordMD5 cryptPasswordMD5 = new CryptPasswordMD5();

	@Override
	public int register(Candidate c) {
		em.persist(c);
		c.setPassword(cryptPasswordMD5.cryptWithMD5(c.getPassword()));

		c.setActivated(false);
		String destmail = c.getEmail();
		String message = "Hi" + c.getNom()
				+ "\n you registered to our website pliz click on the link to activate your acount"
				+ " http://localhost:9080/jobs.pi-web/User/activate/" + c.getId();
		String subject = "Activation";
		mail.sendMail(destmail, message, subject);
		return c.getId();

	}

	@Override
	public void activate(int id) {
		User u = em.find(User.class, id);
		u.setSetActivated(true);
	}

	@Override
	public int registerE(Employe e) {
		em.persist(e);
		e.setPassword(cryptPasswordMD5.cryptWithMD5(e.getPassword()));
		return e.getId();

	}

	@Override
	public List<User> ListeUsers() {
		List<User> u = em.createQuery("SELECT x FROM User x", User.class).getResultList();

		return u;
	}



	public int registerA(SuperAdmin sa) {
		em.persist(sa);
		sa.setPassword(cryptPasswordMD5.cryptWithMD5(sa.getPassword()));
		return sa.getId();
	}

	
	@Override
	public User findUserById(int id) {
		User e = em.find(User.class, id);
		return e;
	}
	
	@Override
	public List<Candidate> findCandidateByName(String name) {
		Query query = em.createQuery("select c from Candidate c where c.nom like :nnom", Candidate.class);
		query.setParameter("nnom", "%" + name + "%");
		return query.getResultList();
	}
	@Override
	public int updateUser(User u) {
		
		em.merge(u);
		return 0;
	}
	
	@Override
	public void AddSkilltoCandidate(int idSkill,int idCandidate)
	{
		
		CandidateSkills cs = em.find(CandidateSkills.class, idSkill);
		Candidate c = em.find(Candidate.class, idCandidate);
		System.out.println("******************************"+c.getId());		
		Set<CandidateSkills> css = c.getSkills();
		css.add(cs);
		c.setSkills(css);		
		
	}
	@Override
	public List<Candidate> findCandidateBySkill(String skill) {
		
		
		
		List<Candidate> c = em.createQuery("select c from Candidate c", Candidate.class).getResultList();
		List<Candidate> cc = new ArrayList<Candidate>();
		for (Candidate candidate : c) {
			for(CandidateSkills skil:candidate.getSkills()) {
				System.out.println(skil.getSkill());
				System.out.println(skill);
				if(skil.getSkill().equals(skill)) {
					System.out.println(skil.getSkill());
					cc.add(candidate);
					
				}
			}
			

			
		}
		System.out.println(cc);
		return cc; 
	}
	@Override
	public Set<Candidate> getMyContacts(int id) {
		Query qc = em.createNativeQuery("SELECT *  FROM User where (SELECT Contact_id FROM user_user where Candidate_id=:id)");
		qc.setParameter("id", id);
		Candidate c = em.find(Candidate.class,19);
		return (Set<Candidate>) qc;
	}

}
