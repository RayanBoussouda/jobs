package jobs.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import jobs.interfaces.AuthenticationServiceRemote;
import jobs.model.Candidate;
import jobs.model.User;
import jobs.utilities.CryptPasswordMD5;

@Stateless
public class AuthenticationService implements AuthenticationServiceRemote {
	@PersistenceContext(unitName = "jobs-ejb")
	EntityManager em;
	public static User LoggedPerson;

	private CryptPasswordMD5 cryptPasswordMD5 = new CryptPasswordMD5();
//
//	@Override
//	public int ajouterMission(Candidate cd) {
//		em.persist(cd);
//		return cd.getId();
//	}


	@Override
	public User authentificationUser(String email, String password) {
		User person = new User();
		try {
			TypedQuery<User> query = em.createQuery("SELECT p FROM User p where p.email=:email", User.class);
			query.setParameter("email", email);
			person = query.getSingleResult();

			if (person.getPassword().equals(cryptPasswordMD5.cryptWithMD5(password))&&person.getSetActivated()==true) {

				LoggedPerson = person;
				System.out.println(person.getId());

				return LoggedPerson;

			} else {
				System.out.println(person);
				return new User();
			}

		} catch (NoResultException n) {
			System.out.println("err");
		}

		System.out.println(person);
		return person;

	}
}
