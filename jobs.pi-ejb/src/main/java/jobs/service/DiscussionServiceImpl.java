
package jobs.service;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import jobs.interfaces.DiscussionService;
import jobs.model.Comment;
import jobs.model.Discussion;
import jobs.model.Message;
import jobs.model.Notification;
import jobs.model.Post;
import jobs.model.User;
import jobs.utilities.Utilities;



@Stateless
@LocalBean
public class DiscussionServiceImpl implements DiscussionService {



	@PersistenceContext(unitName = "jobs-ejb")
	EntityManager em;
	@Override
	public void addDiscussion(Discussion disc) {
		em.persist(disc);
		em.flush();
	}
	

	@Override
	public List<Discussion> viewAllDiscussion() {
		TypedQuery<Discussion> query = em.createQuery("SELECT disc FROM Discussion disc  ORDER BY disc.updated_at DESC",Discussion.class);	
		List<Discussion> p = query.getResultList();
		System.out.println(p);
		return p;
	}
	@Override
	public List<Discussion> viewAllUserDiscussion(int userId) {
		TypedQuery<Discussion> query = em.createQuery("SELECT disc  FROM Discussion disc WHERE (disc.user1.id= :userId) OR (disc.user2.id= :userId)  ORDER BY disc.updated_at DESC",Discussion.class);
		query.setParameter("userId", userId); 
		List<Discussion>p = query.getResultList();
		return p;
	}

	@Override
	public void sendMessage(Message message, int senderId, int receiverId) {
		TypedQuery<Discussion> query;
		Discussion disc;
		try {
			message.setCreated_at(Utilities.getTodayDate());
			query=em.createQuery("SELECT disc FROM Discussion disc where  ((disc.user1.id = :senderId and disc.user2.id = :receiverId) OR (disc.user1.id = :receiverId and disc.user2.id = :senderId))  ",Discussion.class);
			query.setParameter("senderId", senderId).setParameter("receiverId", receiverId);
			List<Discussion> discussions= query.getResultList();
			System.out.println(discussions);
			User sender = em.find(User.class, senderId); 
			User receiver = em.find(User.class, receiverId);
			if (discussions.isEmpty())
			{
				disc = new Discussion();
				disc.setCreated_at(Utilities.getTodayDate());
				disc.setUser1(sender);
				disc.setUser2(receiver);
				disc.setUpdated_at(Utilities.getTodayDate());
				addDiscussion(disc);
				message.setSenderId(senderId);
				message.setDiscussion(getDiscussionById(disc.getId()));
				message.setCreated_at(Utilities.getTodayDate());
				em.persist(message);
				em.flush();
			}
			else
			{
				disc=discussions.get(0);
				disc.getMessages().add(message);
				disc.setUpdated_at(Utilities.getTodayDate());
				message.setSenderId(senderId);
				message.setDiscussion(disc);
				message.setCreated_at(Utilities.getTodayDate());
				em.persist(message);
				em.merge(disc);
				em.flush();
			}
			Notification notif=new Notification();

			notif.setSeen(0);
			notif.setTitle("New Message");
			notif.setDate(Utilities.getTodayDate());
			notif.setObject("Mr/Mm "+receiver.getPrenom()+" " +sender.getPrenom() +" sent you a message");
			em.persist(notif);


		}catch (Exception e) {
			System.out.println(e.getMessage());

		}


	}





	

	@Override
	public List<Message> viewAllMessageDiscussion(int discussionId) {
		List<Message> p;
		TypedQuery<Message> query = em.createQuery("SELECT msgs FROM Message msgs WHERE (msgs.discussion.id= :idDisc)   ORDER BY msgs.created_at ASC",Message.class);
		query.setParameter("idDisc", discussionId);
		p = query.getResultList();
		return p;

	}
	


	@Override
	public List<Message> viewAllMessage() {
		List<Message> p;
		TypedQuery<Message> query = em.createQuery("SELECT msgs FROM Message msgs   ORDER BY msgs.created_at ASC",Message.class);
		
		p = query.getResultList();
		return p;

	}


	@Override
	public void deleteMessage(int messageId) {
		Message p= em.find(Message.class, messageId);
		em.remove(p);
	
	}


	@Override
	public Discussion getDiscussionById(int discussionId) {
		return em.find(Discussion.class, discussionId);
	}



	@Override
	public Message getMessageById(int messageId) {
		return em.find(Message.class, messageId);
	}

	@Override
	public List<Message> viewAllMessageDiscussion(int  idDiscussion ,int userId) {
		Discussion disc=getDiscussionById(idDiscussion);
		List<Message> p;
		if(disc.getUser1().getId()==userId)
		{
			TypedQuery<Message> query = em.createQuery("SELECT msgs FROM Message msgs WHERE (msgs.discussion.id= :idDisc) AND (msgs.user1Deleted= false)  ORDER BY msgs.created_at ASC",Message.class);
			query.setParameter("idDisc", idDiscussion);
			p = query.getResultList();

		}
		else
		{
			TypedQuery<Message> query = em.createQuery("SELECT msgs FROM Message msgs WHERE (msgs.discussion.id= :idDisc) AND (msgs.user2Deleted= false)  ORDER BY msgs.created_at ASC",Message.class);
			query.setParameter("idDisc", idDiscussion);
			p = query.getResultList();
		}
		updateSeenOnMessages(idDiscussion,userId);
		return p;
	}

	@Override
	public void deleteConversation(int userId, int conversationId) {
		Discussion disc=getDiscussionById(conversationId);
		if (disc.getUser1().getId()==userId)
		{
			disc.getMessages().stream().forEach(m -> m.setUser1Deleted(true));
		}
		else 
		{
			disc.getMessages().stream().forEach(m -> m.setUser2Deleted(true));
		}
		em.merge(disc);
		em.flush();

	}


	@Override
	public void checkDeletedMessages() {
		TypedQuery<Message> query = em.createQuery("SELECT msgs FROM Message msgs WHERE (msgs.user2Deleted= true AND msgs.user1Deleted= true) ",Message.class);
		List<Message> msgs = query.getResultList();
		System.out.println(msgs);
		for (Message msg : msgs)
		{
			System.out.println( msg.getId());
			em.remove(em.find(Message.class, msg.getId()));
			em.flush();
		}
	}

	@Override
	public void updateSeenOnMessages(int discussionId , int receiverId) {
		try {
			Discussion c = em.find(Discussion.class, discussionId);
			c.getMessages().stream().filter(m -> m.getSeen() == null && m.getSenderId() != receiverId)
			.forEach(m -> m.setSeen(Utilities.getTodayDate()));

			em.merge(c);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
