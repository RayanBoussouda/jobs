package jobs.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;

import jobs.interfaces.SheetQuickstartServiceRemote;
import jobs.model.Event;
import jobs.model.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class SheetsQuickstart implements SheetQuickstartServiceRemote {
	@PersistenceContext
	EntityManager em;
	private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = SheetsQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	/**
	 * Prints the names and majors of students in a sample spreadsheet:
	 * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
	 */
	@Override
	public List<List<Object>> getsheet() throws IOException, GeneralSecurityException {
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		final String spreadsheetId = "1P938xkBb2aR9LlmaGvldeZHk57XMJl5gPDVPySNTtZU";
		final String range = "A2:G";
		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values = response.getValues();
		return values;
//        if (values == null || values.isEmpty()) {
//            System.out.println("No data found.");
//        } else {
//            System.out.println("Name, Major");
//            for (List row : values) {
//            	System.out.println(row.get(1));
//           
//            }
//        }
	}

	@Override
	public Map<String,Map<String,Integer>> getStatsOfEvent(int idEvent) throws IOException, GeneralSecurityException {
		Event ev = em.find(Event.class, idEvent);
		Set<User> u = ev.getUsers();
		List<List<Object>> sheet = getsheet();
		List<List<Object>> sheetEvent = new ArrayList<List<Object>>();
		System.out.println(u);
		System.out.println(sheet.size());
		for (User user : u) {
			for (int i = 0; i < sheet.size(); i++) {
				if (user.getEmail().equals(sheet.get(i).get(1))) {
					sheetEvent.add(sheet.get(i));
				}
			}
		}
		System.out.println(sheetEvent);
		int notAtAlllikely,likely,reallyLikely,poor,fair,excellent,verryGood,veryOrganized,somewhatOrganized,notOrganized,someFriendly,veryFriendly,notFriendly,tooLong,aboutRight,tooShort;
		notAtAlllikely=likely=reallyLikely=poor=excellent=fair=verryGood=veryOrganized=somewhatOrganized=notOrganized=someFriendly=veryFriendly=notFriendly=tooLong=aboutRight=tooShort=0;
		for (int i = 0; i < sheetEvent.size(); i++) {
			if(sheetEvent.get(i).get(2).equals("Not at all likely")) {
				notAtAlllikely++;
			}else if (sheetEvent.get(i).get(2).equals("likely")) {
				likely++;
			}else if (sheetEvent.get(i).get(2).equals("Really likely")) {
				reallyLikely++;
			}
			if(sheetEvent.get(i).get(3).equals("poor")) {
				poor++;
			}else if(sheetEvent.get(i).get(3).equals("fair")) {
				fair++;
			}else if(sheetEvent.get(i).get(3).equals("Excellent")) {
				excellent++;
			}else if(sheetEvent.get(i).get(3).equals("Verry good")) {
				verryGood++;
			}
			if(sheetEvent.get(i).get(4).equals("Very organized")) {
				veryOrganized++;
			}else if(sheetEvent.get(i).get(4).equals("Somewhat organized")) {
				somewhatOrganized++;
			}else if (sheetEvent.get(i).get(4).equals("Not at all organized")) {
				notOrganized++;
			}
			if(sheetEvent.get(i).get(5).equals("Somewhat Friendly")) {
				someFriendly++;
			}else if(sheetEvent.get(i).get(5).equals("Very Friendly")) {
				veryFriendly++;
			}else if(sheetEvent.get(i).get(5).equals("Not So Friendly")) {
				notFriendly++;
			}
			if(sheetEvent.get(i).get(6).equals("Too long")) {
				tooLong++;
			}else if(sheetEvent.get(i).get(6).equals("About right"))  {
				aboutRight++;
			}else if(sheetEvent.get(i).get(6).equals("Too short"))  {
				tooShort++;
			}
			
		}
		Map<String, Integer> hm = new HashMap<String, Integer>();
		hm.put("Not at all likely", notAtAlllikely);
		hm.put("likely", likely);
		hm.put("Really likely", reallyLikely);
		Map<String, Integer> hm2 = new HashMap<String, Integer>();
		hm2.put("poor", poor);
		hm2.put("fair", fair);
		hm2.put("Excellent", excellent);
		hm2.put("Verry Good", verryGood);
		Map<String, Integer> hm3 = new HashMap<String, Integer>();
		hm3.put("Very organized", veryOrganized);
		hm3.put("Somewhat organized", somewhatOrganized);
		hm3.put("Not at all organized", notOrganized);
		Map<String, Integer> hm4 = new HashMap<String, Integer>();
		hm4.put("Somewhat Friendly", someFriendly);
		hm4.put("Very Friendly", veryFriendly);
		hm4.put("Not So Friendly", notFriendly);
		Map<String, Integer> hm5 = new HashMap<String, Integer>();
		hm5.put("Too long", tooLong);
		hm5.put("About right", aboutRight);
		hm5.put("Too short", tooShort);
		Map<String,Map<String,Integer>> map = new HashMap<String, Map<String,Integer>>();
		map.put("How likely is it that you would recommend the event to a friend or collegue ?",hm);
		map.put("Overrall, how would you rate the event?", hm2);
		map.put("How organized was the event?", hm3);
		map.put("How friendly was the staff ?", hm4);
		map.put("Was the event legth too long, too short or about right ?", hm5);
		return map;
	}
}
