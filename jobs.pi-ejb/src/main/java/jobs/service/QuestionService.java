package jobs.service;

import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jobs.interfaces.QuestionServiceLocal;
import jobs.model.CandidateSkills;
import jobs.model.Question;
import jobs.model.Quiz;

@Stateless
public class QuestionService implements QuestionServiceLocal {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public int addQuestion(int id,Question q) {
		if(q.getAnswers()==null) return -2;
		if(q.getCorrectAnswer()<0 || q.getCorrectAnswer()>q.getAnswers().size()-1)
			return 0;
		CandidateSkills skill = em.find(CandidateSkills.class, id);
		if(skill==null) return -1;
		q.setSkill(skill);
		em.persist(q);
		
		return q.getId();
	}

	@Override
	public int editQuestion(Question q) {
		
		em.merge(q);
		
		return q.getId();
		
	}

	@Override
	public Vector<Question> listQuestions(String q) {
		
		return null;
	}

	@Override
	public boolean deleteQuestion(int id) {
		
		em.remove(em.find(Question.class, id));
		return false;
	}

	@Override
	public Question findQuestion(int id) {
		
		return em.find(Question.class, id);
	}

	@Override
	public List<Question> findAllQuestion(int id) {
		Query query = em.createQuery("select q from Question q where q.skill.id="+id);
		List<Question> trouve= (List<Question>) query.getResultList();
		if(trouve.size()==0) return null;
		return trouve;
		
	}
	
}
